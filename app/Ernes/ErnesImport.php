<?php


namespace App\Ernes;

class ErnesImport
{

    public $counter = 0; //number of added ads
    public $additional_notice;
    public $number = 5;
    public $response = 'html'; //html or cron

    public function setNumber($number)
    {
        $this->number = $number;
    }

    public function setResponse($response)
    {
        $this->response = $response;
    }

    public function import_response()
    {
        if($this->response == 'html') {
            echo 'eeeee';

        }else{
            echo $this->counter.' were added to DB';
        }

    }

    public function import()
    {
        //get last ID
        $last_imported_id = \Import::orderBy('id', 'desc')->first();

        //import
        $records = \Frecord::where('id','>', $last_imported_id->record_id)->with('subrecords')->take($this->number)->get();
        //$edition = Edition::where('start', '<=', \Carbon\Carbon::now())->where('end', '>', \Carbon\Carbon::now())->first();
        $edition = \Edition::find(1);
        foreach($records as $record) {
            $ad = new \Ad();
            $ad->imported_id = $record->id;
            $ad->ip = $record->ip;
            $ad->locked_id = 1;
            $ad->last_editor = 1;
            $ad->last_edition_id = $edition->id;
            $ad->ending_at = $edition->end;
            $ad->created_at = $record->submited;

            $combo_search = '';

            foreach($record->subrecords as $sub) {
                if($sub->element == '6') {
                    $ad->ad_text = $sub->value;
                    $combo_search.= ' '.$sub->value;
                }
                if($sub->element == '7') {
                    $ad->price = $sub->value;
                }
                if($sub->element == '8') {
                    $ad->phone_1 = $sub->value;
                    $combo_search.= ' '.$sub->value;
                }
                if($sub->element == '9') {
                    $ad->phone_2 = $sub->value;
                    $combo_search.= ' '.$sub->value;
                }
                if($sub->element == '1') {
                    //find category
                    $internal_category = \InternalCategory::where('name', 'LIKE', $sub->value.'%')->first();
                    if(is_object($internal_category)) {
                        $ad->internal_category_id = $internal_category->id;
                    }else{
                        $ad->internal_category_id = 0;
                    }

                }
                if($sub->element == '223') {
                    $ad->contact_email = $sub->value;
                    $combo_search.= ' '.$sub->value;
                }
                $ad->combo_search = $combo_search;
            }
            if(!empty($ad->ad_text)) {
                $ad->save();
                $this->counter++;
            }


            $new_last_imported = new \Import();
            $new_last_imported->record_id = $record->id;
            $new_last_imported->save();
        }
        if($this->counter==0) {
            $this->additional_notice = "<br/><b>Dlaczego 0 ogłoszeń?</b> - w bazie znajdują się ogłoszenia z nieobsługiwanego formularza - te ogłoszenia nie są importowane.";
            $this->additional_notice.= "<br/>System importuje na raz ".$this->number." ogłoszeń w kolejności od najstarszych. Dopiero po zaimportowaniu można określić formularz - jeśli jest odpowiedni - ogłoszenie zostaje zapisane.";
            $this->additional_notice.= "<br/><br/>Śmiało - importuj dalej.";
        }else{
            $this->additional_notice = '';
        }
        $this->import_response();
    }

}