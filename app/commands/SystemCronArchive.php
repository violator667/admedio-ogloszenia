<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SystemCronArchive extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'systemcron:archive';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Archives ads.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $now = \Carbon\Carbon::now()->toDateTimeString();
		$ads = Ad::where('active', 1)->where('archive', 0)->where('ending_at', '<', $now)->get();
        if(!$ads->isEmpty()) {
            foreach($ads as $ad) {
                $ad->active = 0;
                $ad->archive = 1;
                $ad->archived_at = $now;
                $ad->save();

                //unexport
                App::make('AdController')->unexport($ad->id);
            }
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(

		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(

		);
	}

}
