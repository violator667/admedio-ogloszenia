<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SystemCronDelete extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'systemcron:delete';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Deletes archived ads.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $now = \Carbon\Carbon::now();
        $now_plus = $now->addDays($this->argument('days'))->toDateTimeString();

        $timelimit = \Carbon\Carbon::parse($this->argument('days').' days ago')->toDateTimeString();

        $ads = Ad::where('archive', 1)->where('archived_at', '<', $timelimit)->get();
        if(!$ads->isEmpty()) {
            foreach($ads as $ad) {
                $ad->delete();
            }
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('days', InputArgument::REQUIRED, 'How many days should ad be in archive to delete it.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(

		);
	}

}
