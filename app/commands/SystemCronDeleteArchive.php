<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SystemCronDeleteArchive extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'systemcron:deleteArchive';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Deletes all archived ads.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $ads = Ad::where('archive', 1)->get();
        if(!$ads->isEmpty()) {
            foreach($ads as $ad) {
                $ad->delete();
            }
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(

		);
	}

}
