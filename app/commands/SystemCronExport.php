<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SystemCronExport extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'systemcron:export';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Runs to export selected ads to TXT';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

        try {
            $task = ExportTxt::where('finished',0)->orderBy('id', 'ASC')->firstOrFail();

//            $ads = Ad::where('active', 1)->where('created_at', '>=', $task->start)->where('ending_at', '>', $task->end)->get()->all();
//            $ads = Ad::where('starting_at', '>=', $task->start)->where('ending_at', '>=', $task->end)->get()->all();
            #1
//            $ads = Ad::where('ending_at', '>=', $task->start)->where('ending_at', '>=', $task->end)->get()->all();

            #2
          $ads = Ad::where('ending_at', '>=', $task->start)->where('starting_at', '<=', $task->end)->get()->all();

//            $queries = DB::getQueryLog();
//            $queries = end($queries);
//
//            print_r($queries);
            $content='';
            foreach ($ads as $record) {
                $cat_name = trim(preg_replace('/\s+/', ' ', $record->internalCategory->name));
                $content .= $cat_name . ', ' . $record->ad_text . ' ';
                if (!empty($record->price)) {
                    $content .= 'cena ' . $record->price . '.';
                }
                if (!empty($record->phone_1)) {
                    $content .= ' Tel. ' . $record->phone_1;
                }
                if (!empty($record->phone_2)) {
                    $content .= ', ' . $record->phone_2;
                }
                if (!empty($record->phone_3)) {
                    $content .= ', ' . $record->phone_3;
                }
                if (!empty($record->place)) {
                    $content .= ', ' . $record->place;
                }
                $content .= "\r\n";
            }
                $filename_base = rand(1000000, 99999999999);
                $filename = $filename_base.'.txt';
                $path = app_path();
                $path.= '/storage/export/';
                $file = File::put($path . $filename, $content);

                $link = Config::get('app.url').'/panel/ads/export/getfile/'.$filename_base;

                $link = '<a href="'.$link.'">'.$link.'</a>';

                $message = new Message();
                $message->subject = 'Twój plik eksportu jest gotowy do pobrania';
                $message->body = 'Plik z danymi ogłoszeń eksportu dla dat:<br/><b>'.$task->start.'</b> do <b>'.$task->end.'</b> jest gotów.<br/>Można go pobrać klikając w ten link:<br>'.$link;
                $message->user_id = 1;
                $message->sender_name = 'SYSTEM';
                $message->reciever_id = $task->user_id;
                $message->reciever_name = $task->user->name;
                $message->save();

                $task->finished_at = date('Y-m-d H:i:s');
                $task->finished = 1;
                $task->save();

        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {

        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
