<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SystemCronImport extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'systemcron:import';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import Ads from DB';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $import = new App\Ernes\ErnesImport();
        $import->setNumber($this->argument('number'));
        $import->import();
        echo $import->number.' ads were imported';
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
            array('number', InputArgument::REQUIRED, 'How many ads should be imported'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(

		);
	}

}
