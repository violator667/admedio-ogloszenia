<?php

class AdController extends \BaseController
{

    /**
     * Display a listing of the resource.
     * GET /ad
     *
     * @return Response
     */
    public function index($option = NULL)
    {
        if (Input::get('display_number')) {
            Session::put('display_number', Input::get('display_number'));
            $display_number = Input::get('display_number');
        } else {
            if (!Session::get('display_number')) {
                $display_number = 10;
            } else {
                $display_number = Session::get('display_number');
            }

        }
        $users_list = User::where('id', '!=', 1)->get();
        $category_list = InternalCategory::all();

        try {
            $current_edition = Edition::where('start', '<=', \Carbon\Carbon::now())->where('end', '>', \Carbon\Carbon::now())->firstOrFail();
            $editions = Edition::where('id', '>=', $current_edition->id)->get()->all();
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $editions = new \Illuminate\Support\Collection();
        }

        //prepare search
        $where =  array();
        if(!empty(Session::get('search_user')) && Session::get('search_user')!=0) {
            $where['last_editor'] = Session::get('search_user');
        }
        if(!empty(Session::get('search_category')) && Session::get('search_category')!=0) {
            $where['internal_category_id'] = Session::get('search_category');
        }

        if(!empty(Session::get('search_text'))) {
            $search_text = Session::get('search_text');
        }else{
            $search_text = '';
        }
        if(!empty(Session::get('search_start'))) {
            $search_start = Session::get('search_start').' 00:00:00';
        }else{
            $search_start = '1970-01-01';
        }
        if(!empty(Session::get('search_end'))) {
            $search_end = Session::get('search_end').' 23:59:59';
        }else{
            $search_end = '2100-12-31';
        }
        if(!empty(Session::get('search_field'))) {
            $search_field = Session::get('search_field');
        }else{
            $search_field = 'id';
        }
        if(!empty(Session::get('search_direction'))) {
            $search_direction = Session::get('search_direction');
        }else{
            $search_direction = 'DESC';
        }


        if (!$option) {
            $allAds = Ad::where($where)->where('combo_search', 'LIKE', '%'.$search_text.'%')->where('updated_at', '>=', $search_start)->where('updated_at', '<=', $search_end)->orderBy($search_field,$search_direction)->paginate($display_number);
            $submenu['text'] = 'Wszystkie';
            $submenu['link'] = '';
            $menuPosition = 'ads';
        } elseif ($option == 'new') {
            $allAds = Ad::where($where)->where('combo_search', 'LIKE', '%'.$search_text.'%')->where('updated_at', '>=', $search_start)->where('updated_at', '<=', $search_end)->orderBy($search_field,$search_direction)->where('last_editor', '1')->paginate($display_number);
            $submenu['text'] = 'Do edycji';
            $submenu['link'] = $option;
            $menuPosition = 'ads';
        } elseif ($option == 'active') {
            $allAds = Ad::where($where)->where('combo_search', 'LIKE', '%'.$search_text.'%')->where('updated_at', '>=', $search_start)->where('updated_at', '<=', $search_end)->orderBy($search_field,$search_direction)->where('active', '1')->paginate($display_number);
            $submenu['text'] = 'Aktualne';
            $submenu['link'] = $option;
            $menuPosition = 'ads';
        } elseif ($option == 'archived') {
            $allAds = Ad::where($where)->where('combo_search', 'LIKE', '%'.$search_text.'%')->where('updated_at', '>=', $search_start)->where('updated_at', '<=', $search_end)->orderBy($search_field,$search_direction)->where('archive', '1')->paginate($display_number);
            $submenu['text'] = 'Archiwum';
            $submenu['link'] = $option;
            $menuPosition = 'ads';
        } elseif ($option == 'exported') {
            $allAds = Ad::where($where)->where('archive', 0)->where('combo_search', 'LIKE', '%'.$search_text.'%')->where('updated_at', '>=', $search_start)->where('updated_at', '<=', $search_end)->orderBy($search_field,$search_direction)->where('exported', '1')->paginate($display_number);
            $submenu['text'] = 'Wyeksportowane';
            $submenu['link'] = $option;
            $menuPosition = 'ads-internet';
        } else {
            $allAds = Ad::where($where)->where('combo_search', 'LIKE', '%'.$search_text.'%')->where('updated_at', '>=', $search_start)->where('updated_at', '<=', $search_end)->orderBy($search_field,$search_direction)->paginate($display_number);
            $submenu['text'] = 'Wszystkie';
            $submenu['link'] = '';
            $menuPosition = 'ads';
        }

        $pagination = $allAds->links();
        $number_of_items = $allAds->getTotal();

        if( (Entrust::hasRole('Administrator')) OR (Entrust::hasRole('VIP'))  ) {
            $locked_admin_info = 'ok';
        }else{
            $locked_admin_info = 'no';
        }

        if (Input::get('page') > $allAds->getLastPage()) {
            return \Redirect::to($allAds->getUrl(1));
        }else{
            return View::make('ad_index')->with('menuPosition', $menuPosition)->with('display_number', $display_number)->with('submenu', $submenu)->with('ads', $allAds)->with('number_of_items', $number_of_items)->with('users_list', $users_list)->with('category_list', $category_list)->with('all_editions', $editions)->with('locked_admin_info', $locked_admin_info)->withPagination($pagination);
        }


    }

    public function search($option = NULL)
    {
        Session::set('search_user', Input::get('search_user'));
        Session::set('search_category', Input::get('search_category'));
        Session::set('search_text', Input::get('search_text'));
        Session::set('search_start', Input::get('search_start'));
        Session::set('search_end', Input::get('search_end'));
        Session::set('search_field', Input::get('search_field'));
        Session::set('search_direction', Input::get('search_direction'));

        if($option) {
            return Redirect::to('/panel/ads');
        }else{
            return Redirect::back();
        }

    }

    public function search_reset()
    {
        Session::set('search_user', '');
        Session::set('search_category', '');
        Session::set('search_text', '');
        Session::set('search_start', '');
        Session::set('search_end', '');
        Session::set('search_field', '');
        Session::set('search_direction', '');

        return Redirect::back();
    }


    public function unlock($id, $redir = NULL, $path = NULL)
    {
        try {
            $ad = Ad::findOrFail($id);
            if ($ad->locked_id == Auth::id()) {
                $ad->locked = 0;
                $ad->locked_id = 1;
                $ad->save();
                if ($redir == NULL) {
                    return Redirect::back()->withNotice('Ogoszenie zostało odblokowane.');
                }else{
                    if($path == NULL) {
                        return Redirect::to('/panel/ads')->withNotice('Ogoszenie zostało odblokowane.');
                    }else{
                        return Redirect::to('/panel/ads/'.$path)->withNotice('Ogoszenie zostało odblokowane.');
                    }

                }
            } else {
                return Redirect::back()->withNotice('Ogłoszenie nie zostało odblokowane.');
            }

        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return Redirect::to('/panel/ads')->withNotice('Wybrane ogłoszenie nie istnieje.');
        }
    }

    public function massdelete()
    {
        $data = Input::all();
        if(Request::ajax())
        {
            if(is_array($data['selected'])) {

                $ads = Ad::findMany($data['selected']);
                foreach($ads as $ad) {
                    if($ad->locked == 0) {
                        $ad->delete();
                    }else{
                        if($ad->locked_id == Auth::id()) {
                            $ad->delete();
                        }
                    }
                }
            }
        }
    }

    public function refresh()
    {
        $data = Input::all();
        if(Request::ajax())
        {
            if(is_array($data['selected'])) {

                $ads = Ad::findMany($data['selected']);
                //get last edition
                $new_edition = Edition::find($data['editions']);

                foreach($ads as $ad) {
                        $ad->last_edition_id = $new_edition->id;
                        $ad->ending_at = $new_edition->end;
                        $now = \Carbon\Carbon::now()->toDateTimeString();
                        if($ad->ending_at > $now) {
                            if(($ad->category_export != 0) && ($ad->internal_category_id != 0)) {
                                $ad->active = 1;
                                $ad->archive = 0;

                                $ad->save();
                                if($ad->can_export == "1") {
                                    $this->export($ad->id);
                                }
                            }else{
                                $ad->save();
                            }
                        }else{
                            $ad->save();
                        }
                    $editor = new Editor();
                    $editor->user_id = Auth::id();
                    $editor->name = Auth::user()->name;
                    $editor->ad_id = $ad->id;
                    $editor->save();
                }
            }
        }
    }

	/**
	 * Show the form for creating a new resource.
	 * GET /ads/add
	 *
	 * @return Response
	 */
	public function create()
	{
        $main_categories = Category::where('parent_id', 0)->get();
        $internal_categories = InternalCategory::all();
        try {
            $edition = Edition::where('start', '<=', \Carbon\Carbon::now())->where('end', '>', \Carbon\Carbon::now())->firstOrFail();
            $editions = Edition::where('id', '>=', $edition->id)->get()->all();

        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return Redirect::to('panel/ads/')->withErrors('Nie możesz dodać ogłoszenia! Dodaj najpierw aktualne wydanie do systemu.');
        }

        if(Input::old('category_export')) {
            $cat_helper = App::make('CategoryHelper');
            $cat_parents = $cat_helper->getAllParents(Input::old('category_export'));
            $old_cat = array_reverse($cat_parents);
            $c = Category::find(Input::old('category_export'));
            $old_cat[] = array('name' => $c->name, 'id' => Input::old('category_export'));
        }else{
            $old_cat = array();
        }

        return View::make('ad_add')->with('menuPosition', 'ads-add')->with('main_categories', $main_categories)->with('internal_categories', $internal_categories)->with('edition', $edition)->with('old_cat', $old_cat)->with('all_editions', $editions);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /ad
	 *
	 * @return Response
	 */
	public function store()
	{
        $post_data = Input::all();
        $rules = array(
            'ad_text' => 'required',
            'phone_1' => 'phone_banned|phone_n',
            'phone_2' => 'phone_banned|phone_n',
            'phone_3' => 'phone_banned',
            'last_edition_id' => 'required|not_zero',
            'category_export' => 'required|not_zero',
            'internal_category_id' => 'required|not_zero',
        );
        $messages = array(
            'ad_text.required' => 'Musisz podać treść.',
            'phone_1.phone_banned' => 'Podany telefon #1 znajduje się na liście numerów zastrzeżonych.',
            'phone_2.phone_banned' => 'Podany telefon #2 znajduje się na liście numerów zastrzeżonych.',
            'phone_3.phone_banned' => 'Podany telefon #3 znajduje się na liście numerów zastrzeżonych.',
            'phone_1.phone_n' => 'Podany telefon #1 jest nieprawidłowy (pozostaw pole puste lub wpisz 9cio cyfrowy numer telefonu)',
            'phone_2.phone_n' => 'Podany telefon #2 jest nieprawidłowy (pozostaw pole puste lub wpisz 9cio cyfrowy numer telefonu)',
            'phone_3.phone_n' => 'Podany telefon #3 jest nieprawidłowy (pozostaw pole puste lub wpisz 9cio cyfrowy numer telefonu)',
            'contact_email.email' => 'Musisz podać prawidłowy adres email do kontaktu.',
            'last_edition_id.required' => 'Musisz wybrać datę końca emisji.',
            'last_edition_id.not_zero' => 'Musisz wybrać datę końca emisji.',
            'category_export.required' => 'Musisz wybrać kategorię (e-dział).',
            'category_export.not_zero' => 'Musisz wybrać kategorię (e-dział).',
            'internal_category_id.required' => 'Musisz wybrać kategorię wewnętrzną.',
            'internal_category_id.not_zero' => 'Musisz wybrać kategorię wewnętrzną.'
        );
        $validator = Validator::make($post_data, $rules, $messages);

        if ($validator->fails()) {
            return Redirect::to('/panel/ads/add')->withInput(Input::all())->withErrors($validator);
        } else {
            //$new_last_edition_id = Input::get('actual_edition_id')+Input::get('editions_number')-1;
            try {
                $edition = Edition::findOrFail(Input::get('last_edition_id'));

                if(Input::get('can_export') == "yes") {
                    $can_export = 1;
                }else{
                    $can_export = 0;
                }

                $formobj = new Ad();
                $formobj->can_export = $can_export;
                $formobj->active = 1;
                $formobj->locked_id = 1;
                $formobj->last_editor = Auth::id();
                $formobj->ip = Request::getClientIp(true);
                $formobj->price = Input::get('price');
                $formobj->phone_1 = Input::get('phone_1');
                $formobj->phone_2 = Input::get('phone_2');
                $formobj->phone_3 = Input::get('phone_3');
                $formobj->contact_email = Input::get('contact_email');
                $formobj->place = Input::get('place');
                $formobj->internal_category_id = Input::get('internal_category_id');
                $formobj->category_export = Input::get('category_export');
                $formobj->ad_text = Input::get('ad_text');
                $formobj->note = Input::get('note');
                $formobj->last_edition_id = $edition->id;
                $formobj->ending_at = $edition->end;
                $formobj->combo_search = Input::get('ad_text').' '.Input::get('phone_1').' '.Input::get('phone_2').' '.Input::get('phone_3').' '.Input::get('contact_email');

                $formobj->save();
                $editor = new Editor();
                $editor->user_id = Auth::id();
                $editor->name = Auth::user()->name;
                $editor->ad_id = $formobj->id;
                $editor->save();

                if(Input::get('can_export') == "yes") {
                    $this->export($formobj->id);
                }else{
                    if($formobj->exported_id !=0) {
                        $this->unexport($formobj->id);
                    }
                }

                return Redirect::to('/panel/ads')->withNotice('Ogłoszenie zostało dodane.');

            } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                return Redirect::to('/panel/ads/add')->withInput(Input::all())->withErrors('Wybrano za dużą ilość emisji.');
            }

        }
	}

    public function export_txt()
    {
        return View::make('export_index')->with('menuPosition', 'ads');
    }

    public function export_txt_post()
    {
        $export = new ExportTxt();
        $export->user_id = Auth::id();
        $export->finished = 0;
        $export->start = Input::get('start');
        $export->end = Input::get('end');
        $export->save();

        return Redirect::to('/panel/ads/export/txt')->with('menuPosition', 'ads')->withNotice('Twój eksport został zlecony do przygotowania. Otrzymasz wiadomość gdy będzie gotowy ściągnięcia.');
    }

    public function export_ajax_txt()
    {
        if(Request::ajax()) {
            $data = Ad::whereIn('id',Input::get('selected'))->get();
            $content='';
            foreach ($data as $record) {
                $cat_name = trim(preg_replace('/\s+/', ' ', $record->internalCategory->name));
                $content .=  $cat_name . ', ' . $record->ad_text . ', ';
                if (!empty($record->price)) {
                    $content .= 'Cena: ' . $record->price;
                }
                if (!empty($record->phone_1)) {
                    $content .= ' Tel. ' . $record->phone_1;
                }
                if (!empty($record->phone_2)) {
                    $content .= ', ' . $record->phone_2;
                }
                if (!empty($record->phone_3)) {
                    $content .= ', ' . $record->phone_3;
                }
                $content .= "\r\n";
            }
            $filename = Input::get('filename').'.txt';
            $path = app_path();
            $path.= '/storage/export/';
            $file = File::put($path . $filename, $content);
            return Response::json(array('filename' => $filename));
        }
    }


    public function getfile($filename)
    {
        $file= app_path(). '/storage/export/'.$filename.'.txt';
        return Response::download($file, $filename.'.txt');
    }

	/**
	 * Display the specified resource.
	 * GET /ad/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /ad/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        try {
            $ad = Ad::findOrFail($id);
            if(($ad->locked == 1) && ($ad->locked_id != Auth::id())) {
                return Redirect::to('/panel/ads')->withNotice('To ogłoszenie jest zablokowane przez innego użytkownika.');
            }else{

                $ad->locked = 1;
                $ad->locked_id = Auth::id();
                $ad->timestamps = false;
                $ad->save();
                $main_categories = Category::where('parent_id', 0)->get();
                $internal_categories = InternalCategory::all();

                $current_edition = Edition::where('start', '<=', \Carbon\Carbon::now())->where('end', '>', \Carbon\Carbon::now())->first();

                $editions = Edition::where('id', '>=', $current_edition->id)->get()->all();

                if($ad->category_export !=0) {
                    $cat_helper = App::make('CategoryHelper');
                    $cat_parents = $cat_helper->getAllParents($ad->category_export);
                    $old_cat = array_reverse($cat_parents);
                    $c = Category::find($ad->category_export);
                    $old_cat[] = array('name' => $c->name, 'id' => $ad->category_export);
                }else{
                    $old_cat = array();
                }
                return View::make('ad_edit')->with('menuPosition', 'ads')->with('main_categories', $main_categories)->with('old_cat', $old_cat)->with('internal_categories', $internal_categories)->with('ad',$ad)->with('all_editions', $editions);

            }

        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return Redirect::to('/panel/ads/')->withErrors('Wybrane ogłoszenie nie zostało znalezione');
        }
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /ad/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $post_data = Input::all();
        $rules = array(
            'ad_text' => 'required',
            'phone_1' => 'phone_banned|phone_n',
            'phone_2' => 'phone_banned|phone_n',
            'phone_3' => 'phone_banned',
            'contact_email' => 'email',
            'category_export' => 'required|not_zero',
            'internal_category_id' => 'required|not_zero'
        );
        $messages = array(
            'ad_text.required' => 'Musisz podać treść.',
            'phone_1.phone_banned' => 'Podany telefon #1 znajduje się na liście numerów zastrzeżonych.',
            'phone_2.phone_banned' => 'Podany telefon #2 znajduje się na liście numerów zastrzeżonych.',
            'phone_3.phone_banned' => 'Podany telefon #3 znajduje się na liście numerów zastrzeżonych.',
            'phone_1.phone_n' => 'Podany telefon #1 jest nieprawidłowy (pozostaw pole puste lub wpisz 9cio cyfrowy numer telefonu)',
            'phone_2.phone_n' => 'Podany telefon #2 jest nieprawidłowy (pozostaw pole puste lub wpisz 9cio cyfrowy numer telefonu)',
            'phone_3.phone_n' => 'Podany telefon #3 jest nieprawidłowy (pozostaw pole puste lub wpisz 9cio cyfrowy numer telefonu)',
            'contact_email.email' => 'Musisz podać prawidłowy adres email do kontaktu.',
            'category_export.required' => 'Musisz wybrać kategorię (e-dział).',
            'category_export.not_zero' => 'Musisz wybrać kategorię (e-dział).',
            'internal_category_id.required' => 'Musisz wybrać kategorię wewnętrzną.',
            'internal_category_id.not_zero' => 'Musisz wybrać kategorię wewnętrzną.'
        );
        $validator = Validator::make($post_data, $rules, $messages);

        if ($validator->fails()) {
            return Redirect::to('/panel/ads/'.$id.'/edit')->withInput(Input::all())->withErrors($validator);
        } else {
            try {
                $formobj = Ad::findOrFail($id);
                if(($formobj->locked == 1) && ($formobj->locked_id != Auth::id())) {
                    return Redirect::to('/panel/ads')->withNotice('To ogłoszenie jest zablokowane przez innego użytkownika.');
                }else {

                    if(Input::get('can_export') == "yes") {
                        $can_export = 1;
                    }else{
                        $can_export = 0;
                    }

                    $edition = Edition::findOrFail(Input::get('last_edition_id'));
                    $formobj->can_export = $can_export;

                    $formobj->locked_id = Auth::id();
                    $formobj->last_editor = Auth::id();
                    $formobj->ip = Request::getClientIp(true);
                    $formobj->price = Input::get('price');
                    $formobj->phone_1 = Input::get('phone_1');
                    $formobj->phone_2 = Input::get('phone_2');
                    $formobj->phone_3 = Input::get('phone_3');
                    $formobj->contact_email = Input::get('contact_email');
                    $formobj->place = Input::get('place');
                    $formobj->internal_category_id = Input::get('internal_category_id');
                    $formobj->category_export = Input::get('category_export');
                    if($edition->end > \Carbon\Carbon::now()) {
                        $formobj->archive = 0;
                        $formobj->active = 1;
                    }
                    $formobj->last_edition_id = $edition->id;
                    $formobj->ending_at = $edition->end;
                    $formobj->ad_text = Input::get('ad_text');
                    $formobj->note = Input::get('note');

                    $formobj->combo_search = Input::get('ad_text').' '.Input::get('phone_1').' '.Input::get('phone_2').' '.Input::get('phone_3').' '.Input::get('contact_email');

                    $formobj->save();
                    $editor = new Editor();
                    $editor->user_id = Auth::id();
                    $editor->name = Auth::user()->name;
                    $editor->ad_id = $id;
                    $editor->save();

                    if(Input::get('can_export') == "yes") {
                        $this->export($id);
                    }else{
                        if($formobj->exported_id !=0) {
                            $this->unexport($id);
                        }
                    }

//                    return Redirect::to('/panel/ads/'.$id.'/edit')->withNotice('Ogłoszenie zostało zapisane.');
                    if(Input::get('redir')) {
                        return Redirect::to('/panel/ads/'.$id.'/unlock/redir/'.Input::get('redir'));
                    }else{
                        return Redirect::to('/panel/ads/'.$id.'/unlock/redir');
                    }

                }
            } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                return Redirect::to('/panel/ads/add')->withInput(Input::all())->withErrors('Wybrane ogłoszenie nie istnieje.');
            }

        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        try {
            $ad = Ad::findOrFail($id);
            if(($ad->locked == 1) && ($ad->locked_id != Auth::id())) {
                return Redirect::to('/panel/ads')->withNotice('To ogłoszenie jest zablokowane przez innego użytkownika.');
            }else{
                $ad->delete();
                return Redirect::to('/panel/ads/')->withNotice('Wybrane ogłoszenie zostało usunięte.');
            }

        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return Redirect::to('/panel/ads/')->withErrors('Wybrane ogłoszenie nie zostało znalezione.');
        }
	}

    public function unexport($id)
    {
        $ad = Ad::find($id);
        try {
            $external_ad = ExportRecord::findOrFail($ad->exported_id);
            $external_ad->delete();
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {

        }
        $ad->exported_id = 0;
        $ad->save();
    }


    public function export($id)
    {
        //this exports data to external DB
        $ad = Ad::find($id);
        $now = \Carbon\Carbon::now()->toDateTimeString();
        if($ad->ending_at > $now) {
            if($ad->exported_id!=0) {
                $external_ad = ExportRecord::find($ad->exported_id);
                $save_image = 0;
            }else{
                $external_ad = new ExportRecord();
                $save_image = 1;
            }

            $external_ad->cat_id = $ad->category_export;
            $external_ad->name = Str::limit($ad->ad_text, 40, '');
            $external_ad->alias = Str::slug($external_ad->name);
            $external_ad->description = '<p><span style="color: #414141; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 20px;">'.$ad->ad_text.'</span></p>';
            $external_ad->intro_desc = $ad->ad_text;
            $external_ad->date_start = $ad->created_at;
            $external_ad->date_exp = $ad->ending_at;
            $external_ad->date_mod = $ad->updated_at;
            $external_ad->payed = 1;
            $external_ad->published = 1;
            $external_ad->price = $ad->price;
            $external_ad->contact = $ad->phone_1.', '.$ad->phone_2.', '.$ad->phone_3;
            $external_ad->ip_address = $ad->ip;
            $external_ad->email = $ad->contact_email;
            $external_ad->save();
            if($save_image==1) {
                $img = new ExternalImage();
                $img->item_id = $external_ad->id;
                $img->type = 'item';
                $img->name = 'ogloszenia_gazeta';
                $img->ext = 'jpg';
                $img->path = 'images/';
                $img->caption = 'Ogłoszenie z gazety';
                $img->ordering = '1';
                $img->save();
            }

            $ad->exported_id = $external_ad->id;
            $ad->exported = 1;
            $ad->save();
        }



    }

}