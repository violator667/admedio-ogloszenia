<?php

class CategoryController extends \BaseController
{

    /**
     * Display a listing of the resource.
     * GET /category
     *
     * @return Response
     */
    public function index($parent_id = NULL)
    {
        if(!$parent_id) {
            $allCategories = Category::where('parent_id', '0')->paginate(10);
            $parent = '';
        }else{
            $parent = Category::find($parent_id);
            $allCategories = Category::where('parent_id', $parent_id)->paginate(10);
        }

        $pagination = $allCategories->links();

        return View::make('category_index')->with('menuPosition', 'category')->with('categories', $allCategories)->with('parent', $parent)->withPagination($pagination);
    }

    public function create()
    {
        $post_data = Input::all();
        $validator = Validator::make($post_data, array('name' => 'required'), array('name.required' => 'Pole nazwa kategorii jest wymagane'));
        if ($validator->fails()) {
            return Redirect::to('/panel/category/add')->withInput(Input::all())->withErrors($validator);
        } else {
            //$formobj = Category::find($id);
            $formobj = new Category();
            $formobj->fill($post_data);
            $formobj->save();
            return Redirect::to('/panel/category')->withNotice('Kategoria została dodana.');
        }

    }

    public function update($id)
    {
        $post_data = Input::all();
        $validator = Validator::make($post_data, array('name' => 'required'), array('name.required' => 'Pole nazwa kategorii jest wymagane'));
        if ($validator->fails()) {
            return Redirect::to('/panel/category/add')->withInput(Input::all())->withErrors($validator);
        } else {
            try {
                $formobj = Category::findOrFail($id);

                $formobj->fill($post_data);
                $formobj->save();
                return Redirect::to('/panel/category/' . $id . '/edit')->withNotice('Edycja przebiegła pomyślnie.');
            } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {

                return Redirect::to('/panel/category')->withNotice('Wybrana kategoria nie istnieje.');
            }

        }
    }

    public function catjson($parent_id)
    {
//        if(Request::ajax()) {
            $category = Category::where('parent_id', $parent_id)->get();
            return Response::json($category);
//        }

    }

    public function add()
    {
        return View::make('category_add')->with('menuPosition', 'category');
    }

    public function edit($id)
    {
        try {
            $category = Category::findOrFail($id);
            return View::make('category_edit')->with('category', $category)->with('menuPosition', 'category');
        }catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return Redirect::to('/panel/category')->withNotice('Wybrana kategoria nie istnieje.');
        }


    }

    public function delete()
    {
        $id = Input::get('id');
        try {
            $category = Category::findOrFail($id);
            $category->delete();
            return Redirect::to('/panel/category')->withNotice('Wybrana kategoria została usunięta z systemu.');
        }catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return Redirect::to('/panel/category')->withNotice('Wybrana kategoria nie istnieje.');
        }
    }

}