<?php

class EditionController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /edition
	 *
	 * @return Response
	 */
	public function index()
	{

        $allEditions = Edition::where('id', '!=', '1')->orderBy('id','DESC')->paginate(10);

        $pagination = $allEditions->links();

        return View::make('edition_index')->with('menuPosition', 'edition')->with('editions', $allEditions)->withPagination($pagination);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /edition/add
	 *
	 * @return Response
	 */
	public function create()
	{
        $prev_edition = Edition::orderBy('id', 'DESC')->first();
        if($prev_edition->id==1) {
            $carbon = \Carbon\Carbon::now();
            $carbon->year(date('Y'))->month(date('m'))->day(date('j'))->hour(11)->minute(00)->second(00);
            //$carbon = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $prev_edition->end)->addSecond();
        }else{
            $carbon = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $prev_edition->end)->addSecond();
        }
//        echo $prev_edition->end;

        $carbon_new = $carbon->day.'/'.$carbon->month.'/'.$carbon->year;
//        $carbon_new_2 = $carbon->year.'-'.$carbon->month.'-'.$carbon->day.' '.$carbon->hour.':'.$carbon->minute.':'.$carbon->second;
        $carbon_new_2 = $carbon->year.'-'.$carbon->month.'-'.$carbon->day;
        $carbon_end_tmp = $carbon->addDays(3);
        $carbon_end_tmp->addSecond();
        $carbon_end = $carbon_end_tmp->day.'/'.$carbon_end_tmp->month.'/'.$carbon_end_tmp->year;
//        $carbon_end_2 = $carbon_end_tmp->year.'-'.$carbon_end_tmp->month.'-'.$carbon_end_tmp->day.' '.$carbon->hour.':'.$carbon->minute.':'.$carbon->second;
        $carbon_end_2 = $carbon_end_tmp->year.'-'.$carbon_end_tmp->month.'-'.$carbon_end_tmp->day;
        return View::make('edition_add')->with('menuPosition', 'edition')->with('prev_edition',$prev_edition)->with('start_date', $carbon_new)->with('end_date', $carbon_end)->with('start_date2', $carbon_new_2)->with('end_date2', $carbon_end_2);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /edition/add
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = array('name' => 'required|unique:editions,name',
                        'start' => 'required',
                        'end' => 'required');
        $messages = array(
                            'name.required' => 'Pole nazwa wydania jest wymagane.',
                            'name.unique' => 'Pole nazwa wydania musi być unikalne.',
                            'start.required' => 'Musisz wskazać początek wydania.',
                            'end.required' => 'Musisz wskazać koniec wydania.'
            );
        $post_data = Input::all();
        $validator = Validator::make($post_data, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::to('/panel/edition/add')->withInput(Input::all())->withErrors($validator);
        } else {
            $formobj = new Edition();
            $formobj->fill($post_data);
            $formobj->save();
            return Redirect::to('/panel/edition')->withNotice('Wydanie zostało dodane.');
        }
	}

	/**
	 * Display the specified resource.
	 * GET /edition/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /edition/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        if($id == '1') {
            return Redirect::to('/panel/edition/')->withNotice('Wybrane wydanie nie istnieje.');
        }else{
            try {
                $edition = Edition::findOrFail($id);

                $carbon1 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $edition->start)->addSecond();
                $start_date = $carbon1->day.'/'.$carbon1->month.'/'.$carbon1->year;

                $carbon2 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $edition->end)->addSecond();
                $end_date = $carbon2->day.'/'.$carbon2->month.'/'.$carbon2->year;


                return View::make('edition_edit')->with('menuPosition', 'edition')->with('edition', $edition)->with('start_date', $start_date)->with('end_date', $end_date);
            }catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {

                return Redirect::to('/panel/edition')->withNotice('Wybrane wydanie nie istnieje.');
            }
        }

	}

	/**
	 * Update the specified resource in storage.
	 * PUT /edition/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        if($id == '1') {
            return Redirect::to('/panel/edition/')->withNotice('Wybrane wydanie nie istnieje.');
        }else{
            $rules = array('name' => 'required',
                'start' => 'required',
                'end' => 'required');
            $messages = array(
                'name.required' => 'Pole nazwa wydania jest wymagane.',
                'start.required' => 'Musisz wskazać początek wydania.',
                'end.required' => 'Musisz wskazać koniec wydania.'
            );
            $post_data = Input::all();
            $validator = Validator::make($post_data, $rules, $messages);
            if ($validator->fails()) {
                return Redirect::to('/panel/edition/'.$id.'/edit')->withInput(Input::all())->withErrors($validator);
            } else {
                $formobj = Edition::find($id);
                $formobj->fill($post_data);
                $formobj->save();
                //update ads if they finish on this edition
                $ads = Ad::where('last_edition_id',$id)->get();
                foreach($ads as $ad) {
                    $ad->ending_at = Input::get('end');
                    $ad->save();
                }
                return Redirect::to('/panel/edition/'.$id.'/edit')->withNotice('Wydanie zostało zmienione.');
            }
        }

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /edition/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}