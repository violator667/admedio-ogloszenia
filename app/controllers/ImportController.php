<?php

class ImportController extends \BaseController {

    public function import($number = NULL) {
        $import = new App\Ernes\ErnesImport();
        if($import->import())
        {
            return Redirect::back()->with('notice', 'Importowanie nowych ogłoszeń zakończone. Zaimportowano '.$import->counter.' ogłoszeń.'.$import->additional_notice);
        }else{
            return Redirect::back()->with('error', 'Wystąpił błąd podczas importowania.');
        }
    }
}