<?php

class InternalCategoryController extends \BaseController {

    public function index()
    {

        $allCategories = InternalCategory::paginate(10);


        $pagination = $allCategories->links();

        return View::make('internal_category_index')->with('menuPosition', 'category2')->with('categories', $allCategories)->withPagination($pagination);
    }

    public function create()
    {
        $post_data = Input::all();
        $validator = Validator::make($post_data, array('name' => 'required'), array('name.required' => 'Pole nazwa kategorii jest wymagane'));
        if ($validator->fails()) {
            return Redirect::to('/panel/internal-category/add')->withInput(Input::all())->withErrors($validator);
        } else {
            $formobj = new InternalCategory();
            $formobj->fill($post_data);
            $formobj->save();
            return Redirect::to('/panel/internal-category')->withNotice('Kategoria została dodana.');
        }

    }

    public function update($id)
    {
        $post_data = Input::all();
        $validator = Validator::make($post_data, array('name' => 'required'), array('name.required' => 'Pole nazwa kategorii jest wymagane'));
        if ($validator->fails()) {
            return Redirect::to('/panel/internal-category/add')->withInput(Input::all())->withErrors($validator);
        } else {
            try {
                $formobj = InternalCategory::findOrFail($id);

                $formobj->fill($post_data);
                $formobj->save();
                return Redirect::to('/panel/internal-category/' . $id . '/edit')->withNotice('Edycja przebiegła pomyślnie.');
            } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {

                return Redirect::to('/panel/internal-category')->withNotice('Wybrana kategoria nie istnieje.');
            }

        }
    }


    public function add()
    {
        return View::make('internal_category_add')->with('menuPosition', 'category2');
    }

    public function edit($id)
    {
        try {
            $category = InternalCategory::findOrFail($id);
            return View::make('internal_category_edit')->with('category', $category)->with('menuPosition', 'category2');
        }catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return Redirect::to('/panel/internal-category')->withNotice('Wybrana kategoria nie istnieje.');
        }


    }

    public function delete()
    {
        $id = Input::get('id');
        try {
            $category = InternalCategory::findOrFail($id);
            $category->delete();
            return Redirect::to('/panel/internal-category')->withNotice('Wybrana kategoria została usunięta z systemu.');
        }catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return Redirect::to('/panel/internal-category')->withNotice('Wybrana kategoria nie istnieje.');
        }
    }

}