<?php

class MessageController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /message
	 *
	 * @return Response
	 */
	public function index()
	{
        $allMessages = Message::where('reciever_id','=',Auth::id())->orderBy('id', 'desc')->paginate(10);
        $pagination = $allMessages->links();
		return View::make('messages_inbox')->with('menuPosition', 'message')->with('messages', $allMessages)->withPagination($pagination);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /message/create
	 *
	 * @return Response
	 */
	public function create()
	{

	}

    public function add($id = null)
    {
        $users = User::where('id','!=','1')->where('id', '!=', Auth::id())->get();

        return View::make('messages_add')->with('menuPosition', 'message')->with('users', $users)->with('selected_user',$id);
    }


    public function send()
    {
        $post_data = Input::all();

        Validator::extend('greater_than', function($attribute, $value, $parameters)
        {
            return intval($value) > intval(1);
        });

        $rules = array('to' => 'required|greater_than',
                        'subject' => 'required',
                        'message' => 'required');
        $messages = array('to.required' => 'Musisz wybrać odbiorcę.',
                            'to.greater_than' => 'Musisz wybrać odbiorcę.',
                            'subject.required' => 'Musisz wpisać temat wiadomości',
                            'message.required' => 'Musisz napisać wiadomość.');
        $validator = Validator::make($post_data, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::to('/panel/messages/send-to-user-id/'.Input::get('to'))->withInput(Input::all())->withErrors($validator);
        } else {
            try {
                $reciever = User::findOrFail(Input::get('to'));

                $formobj = new Message();
                $formobj->user_id = Auth::id();
                $formobj->sender_name = Auth::user()->name;
                $formobj->reciever_id = Input::get('to');
                $formobj->reciever_name = $reciever->name;
                $formobj->subject = Input::get('subject');
                $formobj->body = Input::get('message');
                $formobj->save();
                return Redirect::to('/panel/messages')->withNotice('Wiadomość została wysłana.');
            } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                return Redirect::to('/panel/messages')->withNotice('Odbiorca nie istnieje.');
            }
        }
    }

	/**
	 * Store a newly created resource in storage.
	 * POST /message
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /message/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        try {
            $message = Message::findOrFail($id);
            if($message->reciever_id == Auth::id()) {
                $message->read = 1;
                $message->save();
                return View::make('messages_show')->with('menuPosition', 'message')->with('message', $message);
            }else{
                return Redirect::to('/panel/messages')->withNotice('Wybrana wiadomość nie jest do Ciebie.');
            }
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return Redirect::to('/panel/messages')->withNotice('Wybrana wiadomość nie istnieje.');
        }
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /message/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /message/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

    public function delete($id)
    {
        try {
            $message = Message::findOrFail($id);
            if($message->reciever_id == Auth::id()) {
                $message->delete();
                return Redirect::to('/panel/messages')->withNotice('Wiadomośći została usunięta.');
            }else{
                return Redirect::to('/panel/messages')->withNotice('Wybrana wiadomość nie jest do Ciebie.');
            }

        }catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return Redirect::to('/panel/category')->withNotice('Wybrana kategoria nie istnieje.');
        }
    }


	/**
	 * Remove the specified resource from storage.
	 * DELETE /message/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}