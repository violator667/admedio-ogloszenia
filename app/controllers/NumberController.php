<?php

class NumberController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /number
	 *
	 * @return Response
	 */
	public function index()
	{
        $allNumbers = Number::orderBy('id','DESC')->paginate(100);

        $pagination = $allNumbers->links();

        return View::make('number_index')->with('menuPosition', 'numbers')->with('numbers', $allNumbers)->withPagination($pagination);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /number/create
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('number_add')->with('menuPosition', 'numbers');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /number
	 *
	 * @return Response
	 */
	public function store()
	{

        Validator::extend('phone_nn', function($attribute, $value, $parameters)
        {

            if(strlen($value)==9) {
                return 1;
            }else{
                return 0;
            }

        });

        $rules = array('phone' => 'required|numeric|phone_nn'
                    );
        $messages = array(
            'phone.required' => 'Musisz podać numer telefonu.',
            'phone.numeric' => 'Musisz podac prawidłowy numer telefonu.',
            'phone.phone_nn' => 'Numer telefonu musi mieć dokładnie 9 znaków.',
        );
        $post_data = Input::all();
        $validator = Validator::make($post_data, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::to('/panel/numbers/add')->withInput(Input::all())->withErrors($validator);
        } else {
            $formobj = new Number();
            $formobj->fill($post_data);
            $formobj->save();
            return Redirect::to('/panel/numbers')->withNotice('Wydanie zostało dodane.');
        }
	}

	/**
	 * Display the specified resource.
	 * GET /number/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /number/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        try {
            $number = Number::findOrFail($id);
            return View::make('number_edit')->with('menuPosition', 'numbers')->with('number', $number);
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return Redirect::to('/panel/numbers/')->withErrors('Wybrany numer nie został znaleziony.');
        }

	}

	/**
	 * Update the specified resource in storage.
	 * PUT /number/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        Validator::extend('phone_nn', function($attribute, $value, $parameters)
        {

            if(strlen($value)==9) {
                return 1;
            }else{
                return 0;
            }

        });

        $rules = array('phone' => 'required|numeric|phone_nn'
        );
        $messages = array(
            'phone.required' => 'Musisz podać numer telefonu.',
            'phone.numeric' => 'Musisz podac prawidłowy numer telefonu.',
            'phone.phone_nn' => 'Numer telefonu musi mieć dokładnie 9 znaków.',
        );
        $post_data = Input::all();
        $validator = Validator::make($post_data, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::to('/panel/numbers/'.$id.'/edit')->withErrors($validator);
        } else {
            try {
                $formobj = Number::findOrFail($id);

                $formobj->fill($post_data);
                $formobj->save();
                return Redirect::to('/panel/numbers/' . $id . '/edit')->withNotice('Edycja przebiegła pomyślnie.');
            } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {

                return Redirect::to('/panel/numbers')->withNotice('Wybrana kategoria nie istnieje.');
            }
        }
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /number/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        try {
            $number = Number::findOrFail($id);

            $number->delete();
            return Redirect::to('/panel/numbers/')->withNotice('Wybrany numer został usunięty.');


        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return Redirect::to('/panel/numbers/')->withErrors('Wybrany numer nie został znaleziony.');
        }
	}

}