<?php

class PageController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /page
	 *
	 * @return Response
	 */
	public function index()
	{
        $pages = Page::orderBy('id','DESC')->paginate(10);

        $pagination = $pages->links();

        return View::make('page_index')->with('menuPosition', 'cms')->with('pages', $pages)->with('pagination', $pagination);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /page/create
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('page_add')->with('menuPosition', 'cms');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /page
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = array('title' => 'required',
            'body' => 'required');
        $messages = array(
            'title.required' => 'Musisz podać tytuł strony.',
            'body.required' => 'Musisz podać treść strony.'
        );
        $post_data = Input::all();
        $validator = Validator::make($post_data, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::to('/panel/cms/add')->withInput(Input::all())->withErrors($validator);
        } else {
            $formobj = new Page();
            $formobj->fill($post_data);
            $formobj->save();
            return Redirect::to('/panel/cms')->withNotice('Strona została dodana.');
        }
	}

	/**
	 * Display the specified resource.
	 * GET /page/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        try {
            $page = Page::findOrFail($id);

            return View::make('page_show')->with('menuPosition', 'cms')->with('page', $page);
        }catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return Redirect::to('/panel/cms')->withNotice('Wybrana strona nie istnieje.');
        }

	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /page/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

        try {
            $page = Page::findOrFail($id);

            return View::make('page_edit')->with('menuPosition', 'cms')->with('page', $page);
        }catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return Redirect::to('/panel/cms')->withNotice('Wybrana strona nie istnieje.');
        }

	}

	/**
	 * Update the specified resource in storage.
	 * PUT /page/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

        $rules = array('title' => 'required',
            'body' => 'required');
        $messages = array(
            'title.required' => 'Musisz podać tytuł strony.',
            'body.required' => 'Musisz podać treść strony.'
        );
        $post_data = Input::all();
        $validator = Validator::make($post_data, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::to('/panel/cms/'.$id.'/edit')->withInput(Input::all())->withErrors($validator);
        } else {
            $formobj = Page::find($id);
            $formobj->fill($post_data);
            $formobj->save();

            return Redirect::to('/panel/cms/'.$id.'/edit')->withNotice('Strona została zmieniona.');
        }

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /page/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        try {
            $page = Page::findOrFail($id);
            $page->delete();
            return Redirect::back()->withNotice('Wybrana strona została usunięta z systemu.');
        }catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return Redirect::back()->withNotice('Wybrana strona nie istnieje.');
        }
	}

}