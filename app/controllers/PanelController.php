<?php

class PanelController extends \BaseController {


	public function index()
	{
        $new_ads = Ad::where('last_editor', 1)->where('active', 0)->where('archive', 0)->count();
        $active_ads = Ad::where('active', 1)->count();
        $archive_ads = Ad::where('archive', 1)->count();
        $all_ads = Ad::count();

        $cat_num = InternalCategory::all()->count();
        $editions_num = Edition::where('id', '!=', 1)->count();
        $users_num = User::where('id', '!=', 1)->count();
        $mistakes_num = User::where('id', '!=', 1)->sum('mistakes');
        return View::make('panel-index')
            ->with('menuPosition', 'panel')
            ->with('new_ads', $new_ads)
            ->with('active_ads', $active_ads)
            ->with('archive_ads', $archive_ads)
            ->with('all_ads', $all_ads)
            ->with('cat_num', $cat_num)
            ->with('editions_num', $editions_num)
            ->with('users_num', $users_num)
            ->with('mistakes_num', $mistakes_num);
	}



}