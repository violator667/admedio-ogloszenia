<?php



/**
 * UsersController Class
 *
 * Implements actions regarding user management
 */
class UsersController extends Controller
{

    /**
     * Displays the form for account creation
     *
     * @return  Illuminate\Http\Response
     */
    public function create()
    {
        return View::make(Config::get('confide::signup_form'));
    }

    /**
     * Stores new account
     *
     * @return  Illuminate\Http\Response
     */
    public function store()
    {
        $repo = App::make('UserRepository');
        $user = $repo->signup(Input::all());

        if ($user->id) {
            if (Config::get('confide::signup_email')) {
                Mail::queueOn(
                    Config::get('confide::email_queue'),
                    Config::get('confide::email_account_confirmation'),
                    compact('user'),
                    function ($message) use ($user) {
                        $message
                            ->to($user->email, $user->username)
                            ->subject(Lang::get('confide::confide.email.account_confirmation.subject'));
                    }
                );
            }

            return Redirect::action('UsersController@login')
                ->with('notice', Lang::get('confide::confide.alerts.account_created'));
        } else {
            $error = $user->errors()->all(':message');

            return Redirect::action('UsersController@create')
                ->withInput(Input::except('password'))
                ->with('error', $error);
        }
    }

    /**
     * Displays the login form
     *
     * @return  Illuminate\Http\Response
     */
    public function login()
    {
        if (Confide::user()) {
            return Redirect::to('/');
        } else {
            return View::make(Config::get('confide::login_form'));
        }
    }

    /**
     * Attempt to do login
     *
     * @return  Illuminate\Http\Response
     */
    public function doLogin()
    {
        $repo = App::make('UserRepository');
        $input = Input::all();

        if ($repo->login($input)) {
            return Redirect::intended('/');
        } else {
            if ($repo->isThrottled($input)) {
                $err_msg = Lang::get('confide::confide.alerts.too_many_attempts');
            } elseif ($repo->existsButNotConfirmed($input)) {
                $err_msg = Lang::get('confide::confide.alerts.not_confirmed');
            } else {
                $err_msg = Lang::get('confide::confide.alerts.wrong_credentials');
            }

            return Redirect::action('UsersController@login')
                ->withInput(Input::except('password'))
                ->with('error', $err_msg);
        }
    }

    /**
     * Attempt to confirm account with code
     *
     * @param  string $code
     *
     * @return  Illuminate\Http\Response
     */
    public function confirm($code)
    {
        if (Confide::confirm($code)) {
            $notice_msg = Lang::get('confide::confide.alerts.confirmation');
            return Redirect::action('UsersController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_confirmation');
            return Redirect::action('UsersController@login')
                ->with('error', $error_msg);
        }
    }

    /**
     * Displays the forgot password form
     *
     * @return  Illuminate\Http\Response
     */
    public function forgotPassword()
    {
        return View::make(Config::get('confide::forgot_password_form'));
    }

    /**
     * Attempt to send change password link to the given email
     *
     * @return  Illuminate\Http\Response
     */
    public function doForgotPassword()
    {
        if (Confide::forgotPassword(Input::get('email'))) {
            $notice_msg = Lang::get('confide::confide.alerts.password_forgot');
            return Redirect::action('UsersController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_forgot');
            return Redirect::action('UsersController@doForgotPassword')
                ->withInput()
                ->with('error', $error_msg);
        }
    }

    /**
     * Shows the change password form with the given token
     *
     * @param  string $token
     *
     * @return  Illuminate\Http\Response
     */
    public function resetPassword($token)
    {
        return View::make(Config::get('confide::reset_password_form'))
                ->with('token', $token);
    }

    /**
     * Attempt change password of the user
     *
     * @return  Illuminate\Http\Response
     */
    public function doResetPassword()
    {
        $repo = App::make('UserRepository');
        $input = array(
            'token'                 =>Input::get('token'),
            'password'              =>Input::get('password'),
            'password_confirmation' =>Input::get('password_confirmation'),
        );

        // By passing an array with the token, password and confirmation
        if ($repo->resetPassword($input)) {
            $notice_msg = Lang::get('confide::confide.alerts.password_reset');
            return Redirect::action('UsersController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_reset');
            return Redirect::action('UsersController@resetPassword', array('token'=>$input['token']))
                ->withInput()
                ->with('error', $error_msg);
        }
    }

    /**
     * Log the user out of the application.
     *
     * @return  Illuminate\Http\Response
     */
    public function logout()
    {
        Confide::logout();

        return Redirect::to('/');
    }

    public function index()
    {
        $allUsers = User::where('id','!=',1)->paginate(50);
        $pagination = $allUsers->links();

        return View::make('users_index')->with('menuPosition', 'users')->with('users', $allUsers)->withPagination($pagination);
    }

    public function edit($id)
    {
        if($id!=1) {
            try {
                $user = User::with('roles')->findOrFail($id);
                $roles = $user->roles;
                $groups = Role::all();
                $roles_array = [];
                foreach($roles as $role)
                {
                    $roles_array[] = $role->id;
                }
                return View::make('users_edit')->with('menuPosition', 'users')->with('user', $user)->with('roles',$roles_array)->with('groups', $groups);
            }catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {

                return Redirect::to('/panel/users')->withNotice('Wybrany użytkownik nie istnieje.');
            }
        }else{
            return Redirect::to('/panel/users')->withNotice('Wybrany użytkownik nie istnieje.');
        }

    }

    public function update($id)
    {
        $post_data = Input::all();
        $rules = array('email' => 'required|email',
            'name' => 'required');
        $messages = array('email.required' => 'Musisz podać adres email.',
            'email.email' => 'Musisz podać prawidłowy adres email.',
            'name.required' => 'Musisz podać nazwę użytkownika.');

        $validator = Validator::make($post_data, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::to('/panel/users/'.$id.'/edit')->withInput(Input::all())->withErrors($validator);
        } else {
            try {
                $formobj = User::with('roles')->findOrFail($id);

                $formobj->email = Input::get('email');
                $formobj->name = Input::get('name');

                if(Input::get('password')) {
                    $passwordUser = PasswordRepository::find($id);
                    $passwordUser->password = Hash::make(Input::get('password'));
                    $passwordUser->save();
                }

                //delete role assignment
                $formobj->roles()->detach();

                //add new roles
                if(Input::get('group')) {
                    foreach(Input::get('group') as $group) {
                        $thegroup = Role::where('id','=',$group)->first();
                        $formobj->attachRole( $thegroup );
                    }
                }

                $formobj->save();
                return Redirect::to('/panel/users/' . $id . '/edit')->withNotice('Edycja przebiegła pomyślnie.');
            } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {

                return Redirect::to('/panel/users/')->withNotice('Wybrany użytkownik nie istnieje.');
            }

        }
    }

    public function add()
    {
        $groups = Role::all();
        return View::make('users_add')->with('menuPosition', 'users')->with('groups', $groups);
    }

    public function admin_create()
    {
        $post_data = Input::all();
        $rules = array('email' => 'required|email|unique:users',
                        'name' => 'required',
                        'password' => 'required|min:6',
                        'password_confirmation' => 'required|same:password');
        $messages = array('email.required' => 'Musisz podać adres email.',
                            'email.email' => 'Musisz podać prawidłowy adres email.',
                            'email.unique' => 'Podany adres email znajduje się już w bazie - nie możesz użyć go ponownie.',
                            'name.required' => 'Musisz podać nazwę użytkownika.',
                            'password.required' => 'Musisz podać hasło.',
                            'password.min' => 'Hasło musi mieć przynajmiej 6 znaków.',
                            'password_confirmation.required' => 'Musisz potwierdzić hasło.',
                            'password_confirmation.same' => 'Hasła muszą być takie same.');

        $validator = Validator::make($post_data, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::to('/panel/users/add')->withInput(Input::all())->withErrors($validator);
        } else {
//            $repo = App::make('UserRepository');
//            $user = $repo->signup(Input::all());
            $user = new PasswordRepository();
            $user->email = Input::get('email');
            $user->name = Input::get('name');
            $user->password = Hash::make(Input::get('password'));
            $user->save();

            if($user->id) {
                if(Input::get('group')) {
                    $userA = User::find($user->id);
                    foreach(Input::get('group')  as $group) {
                        $thegroup = Role::where('id','=',$group)->first();
                        $userA->attachRole( $thegroup );
                    }
                }

                return Redirect::to('/panel/users/')->withNotice('Użytkownik został utworzony.');

            }else{
//                return Redirect::to('/panel/users')->withErrors(array('errors' => array('default' => array('Wystąpił błąd #1'))));
                return Redirect::to('/panel/users')->withErrors('Wystąpił błąd #1');
            }

        }

    }

    public function mistake($user_id, $ad_id)
    {
        try {
            $reciever = User::findOrFail($user_id);
            $reciever->mistakes = $reciever->mistakes+1;
            $reciever->save();
            $ad = Ad::find($ad_id);
            $message = new Message();
            $message->user_id = Auth::id();
            $message->sender_name = Auth::user()->name;
            $message->reciever_id = $user_id;
            $message->reciever_name = $reciever->name;
            $message->subject = 'Popełniłeś błąd w ogłoszeniu!';
            $message->body = 'Niestety popełniłeś błąd w ogłoszeniu:<br/>'.$ad->ad_text.' '.$ad->price.' '.$ad->phone_1.' '.$ad->phone_2.' '.$ad->phone_3.' '.$ad->contact_email;
            $message->save();

            return Redirect::back()->withNotice('Błąd został zanotowany na koncie użytkownika. Wiadomość została wysłana.');
        }catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return Redirect::back()->withErrors('Wybrany użytkownik nie istnieje.');
        }


    }

    public function delete($id = NULL)
    {
        if(!$id) {
            $id = Input::get('id');
        }

        if($id == '1') {
            return Redirect::to('/panel/users')->withNotice('Wybrany użytkownik nie istnieje.');
        }else{
            try {
                $user = User::findOrFail($id);
                $user->delete();
                return Redirect::to('/panel/users')->withNotice('Wybrany użytkownik został usunięty z systemu.');
            }catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {

                return Redirect::to('/panel/users')->withNotice('Wybrany użytkownik nie istnieje.');
            }
        }

    }
}
