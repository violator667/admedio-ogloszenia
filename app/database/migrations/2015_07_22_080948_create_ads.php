<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAds extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ads', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('exported')->default(0);
            $table->integer('exported_id')->default(0); //ID of ad in external DB (used on edit)
            $table->integer('archive')->default(0);
            $table->integer('locked')->default(0);
            $table->integer('locked_id')->unsigned();
            $table->integer('last_editor')->unsigned();
            $table->string('ip', 100);
            $table->string('price', 50);
            $table->string('phone_1',100);
            $table->string('phone_2',100);
            $table->string('phone_3',100);
            $table->string('name', 100);
            $table->string('surname', 100);
            $table->string('company_name',200);
            $table->string('company_tax_number', 20);
            $table->string('contact_email',150);
            $table->string('category_import');
            $table->integer('category_export')->default(0);
            $table->text('ad_text');

			$table->timestamps();

            $table->foreign('locked_id')->references('id')->on('users');
            $table->foreign('last_editor')->references('id')->on('users');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ads');
	}

}
