<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateAds extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ads', function(Blueprint $table)
		{
			$table->integer('active')->default(0)->after('exported_id');
            $table->integer('last_edition_id')->default(0)->after('ad_text');
            $table->dateTime('ending_at')->after('last_edition_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ads', function(Blueprint $table)
		{
            $table->dropColumn('active');
            $table->dropColumn('last_edition_id')->default(1);
            $table->dropColumn('ending_at');
		});
	}

}
