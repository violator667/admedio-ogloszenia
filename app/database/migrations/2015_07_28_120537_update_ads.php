<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateAds extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ads', function(Blueprint $table)
		{
			$table->dropColumn('name');
			$table->dropColumn('surname');
			$table->dropColumn('company_name');
			$table->dropColumn('company_tax_number');
            $table->dropColumn('category_import');
            $table->integer('internal_category_id')->unsigned()->after('phone_3');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ads', function(Blueprint $table)
		{

		});
	}

}
