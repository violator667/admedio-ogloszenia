<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExport extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('export', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('finished')->default(0);
            $table->dateTime('start');
            $table->dateTime('end');
            $table->timestamp('finished_at');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('export');
	}

}
