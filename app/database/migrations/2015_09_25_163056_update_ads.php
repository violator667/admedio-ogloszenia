<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateAds extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement('ALTER TABLE `ads` CHANGE `imported_id` `imported_id` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 0');
        DB::statement("ALTER TABLE `ads` CHANGE `starting_at` `starting_at` DATETIME NOT NULL DEFAULT '0000-00-00 00:00'");
        DB::statement("ALTER TABLE `ads` CHANGE `ending_at` `ending_at` DATETIME NOT NULL DEFAULT '0000-00-00 00:00'");
        DB::statement("ALTER TABLE `ads` CHANGE `archived_at` `archived_at` DATETIME NOT NULL DEFAULT '0000-00-00 00:00'");

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

	}

}
