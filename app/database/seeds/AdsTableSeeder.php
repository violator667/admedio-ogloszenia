<?php

class AdsTableSeeder extends Seeder {

	public function run()
	{

        foreach(file(app_path().'/database/seeds/korekta86d_eksport.txt') as $line) {
                $data = explode("|",$line);
                $data_count = count($data);
                if($data_count<8) {
                    $d = 8-$data_count;
                    for($i=0;$i<=$d;$i++) {
                        $data[] = '';
                    }
                }
                //search for category
                try{
                    $cat_explode = explode(']',$data[0]);
                    $catnum = $cat_explode[0].']';
                    $cat = InternalCategory::where('name', 'LIKE', $catnum.'%')->firstOrFail();
                    $category_id = $cat->id;
                } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                    $category_id = 0;
                }


                $arr = array(   'imported_id' => 'z pliku',
                    'locked_id' => '1',
                    'last_editor' => '1',
                    'ip' => '127.0.0.1',
                    'price' => $data[2],
                    'phone_1' => $data[3],
                    'phone_2' => $data[4],
                    'phone_3' => $data[5],
                    'internal_category_id' => $category_id,
                    'contact_email' => $data[6],
                    'place' => $data[7],
                    'ad_text' => $data[1],
                    'combo_search' => $data[1].' '.$data[3].' '.$data[4].' '.$data[5].' '.$data[6]
                );
                Ad::create($arr);

        }
	}

}