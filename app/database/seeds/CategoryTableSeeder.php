<?php

// Composer: "fzaninotto/faker": "v1.3.0"
//use Faker\Factory as Faker;

class CategoryTableSeeder extends Seeder {

	public function run()
	{
//		$faker = Faker::create();

        foreach(file(app_path().'/database/seeds/categories.txt') as $line) {
            $data = explode("|",$line);
            $arr = array('name' => $data[0].'] '.$data[1]);
            InternalCategory::create($arr);
        }


	}

}