<?php

// Composer: "fzaninotto/faker": "v1.3.0"
//use Faker\Factory as Faker;

class RolesTableSeeder extends Seeder {

	public function run()
	{
        //create roles

        $administrator = new Role;
        $administrator->name = 'Administrator';
        $administrator->save();

        $biuro = new Role;
        $biuro->name = 'Biuro';
        $biuro->save();

        $biuro_vip = new Role;
        $biuro_vip->name = 'Biuro VIP';
        $biuro_vip->save();

        $vip = new Role;
        $vip->name = 'VIP';
        $vip->save();

        $korekta = new Role;
        $korekta->name = 'Korekta';
        $korekta->save();

        //assign roles
        $user = User::find(2)->first();
        $administrator = Role::where('name','=','Administrator')->first();
        $user->attachRole( $administrator );

        # PANEL ACCESS
        $panel_access = new Permission;
        $panel_access->name = 'panel_access';
        $panel_access->display_name = 'Dostęp do panelu.';
        $panel_access->save();

        #ADS MANAGE
        $ads_manage = new Permission;
        $ads_manage->name = 'ads_manage';
        $ads_manage->display_name = 'Dodawanie, edytowanie, usuwanie ogłoszeń.';
        $ads_manage->save();

        #EXPORT TXT
        $export_txt = new Permission;
        $export_txt->name = 'export_txt';
        $export_txt->display_name = 'Eksport do pliku tekstowego.';
        $export_txt->save();

        #NUMBER MANAGE
        $number_manage = new Permission;
        $number_manage->name = 'number_manage';
        $number_manage->display_name = 'Dodawanie/usuwanie numerów zastrzeżonych z listy.';
        $number_manage->save();

        #MISTAKE MANAGE
        $mistake_manage = new Permission;
        $mistake_manage->name = 'mistake_manage';
        $mistake_manage->display_name = 'Przydzielanie błędów użytkownikom.';
        $mistake_manage->save();

        #CMS MANAGE
        $cms_manage = new Permission;
        $cms_manage->name = 'cms_manage';
        $cms_manage->display_name = 'Edytowanie i dodawanie nowych podstron do systemu (cms).';
        $cms_manage->save();

        #MESSAGE SEND
        $message_send = new Permission;
        $message_send->name = 'message_send';
        $message_send->display_name = 'Możliwość wysyłania komunikatów od użytkowników.';
        $message_send->save();

        #EDITION MANAGE
        $edition_manage = new Permission;
        $edition_manage->name = 'edition_manage';
        $edition_manage->display_name = 'Edytowanie i dodawanie numerów wydań (numer wydania oraz czas jego trwania od - do).';
        $edition_manage->save();

        #USERS MANAGE
        $users_manage = new Permission;
        $users_manage->name = 'users_manage';
        $users_manage->display_name = 'Dodawanie/usuwanie/edycja użytkowników, przypisywanie do grup.';
        $users_manage->save();

        #CATEGORY MANAGE
        $category_manage = new Permission;
        $category_manage->name = 'category_manage';
        $category_manage->display_name = 'Dodawanie/usuwanie/edycja kategorii.';
        $category_manage->save();


        #SYNC
        $administrator = Role::where('name','=','Administrator')->first();
        $administrator->perms()->sync(array($panel_access->id,
            $ads_manage->id,
            $export_txt->id,
            $number_manage->id,
            $mistake_manage->id,
            $cms_manage->id,
            $message_send->id,
            $edition_manage->id,
            $users_manage->id,
            $category_manage->id));

        $biuro = Role::where('name','=','Biuro')->first();
        $biuro->perms()->sync(array($panel_access->id, $ads_manage->id));

        $biuro_vip = Role::where('name','=','Biuro VIP')->first();
        $biuro_vip->perms()->sync(array($panel_access->id,
            $ads_manage->id,
            $number_manage->id,
            $mistake_manage->id,
            $cms_manage->id,
            $message_send->id,
            $edition_manage->id));

        $vip = Role::where('name','=','VIP')->first();
        $vip->perms()->sync(array($panel_access->id,
            $ads_manage->id,
            $message_send->id));

        $korekta = Role::where('name','=','Korekta')->first();
        $korekta->perms()->sync(array($panel_access->id,
            $ads_manage->id,
            $export_txt->id,
            $number_manage->id,
            $mistake_manage->id));
	}

}