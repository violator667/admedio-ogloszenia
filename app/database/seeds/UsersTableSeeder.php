<?php

// Composer: "fzaninotto/faker": "v1.3.0"
//use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
			PasswordRepository::create(['id' => '1', 'name' => 'SYSTEM']);
            PasswordRepository::create(['id' => '2',
                                        'email' => 'do@testow.pl',
                                        'name' => 'Administrator',
                                        'password' => Hash::make('qwerty123')]);
    }

}