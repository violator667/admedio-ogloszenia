<?php
Validator::extend('phone_banned', function($attribute, $value, $parameters)
{
    $number = Number::where('phone', $value)->count();
    if($number > 0) {
        return 0;
    }else{
        return 1;
    }
});
Validator::extend('phone_n', function($attribute, $value, $parameters)
{

    if(strlen($value)==0) {
        return 1;
    }else{
        if(strlen($value)==9) {
            return 1;
        }else{
            return 0;
        }
    }
});
Validator::extend('not_zero', function($attribute, $value, $parameters)
{

    if($value==0) {
        return 0;
    }else{
        return 1;
    }
});