<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('users/login');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/error');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
//	if (Session::token() != Input::get('_token'))
//	{
//		throw new Illuminate\Session\TokenMismatchException;
//	}
    $token = Request::ajax() ? Request::header('X-CSRF-Token') : Input::get('_token');
    if (Session::token() != $token)
    {
        throw new Illuminate\Session\TokenMismatchException;
    }
});


//ROLE FILTERS
Route::filter('panel_access', function()
{
    if (! Entrust::can('panel_access') ) // Checks the current user
    {
        return Redirect::to('/error');
    }
});

Route::filter('category_manage', function()
{
    if (! Entrust::can('category_manage') ) // Checks the current user
    {
        return Redirect::to('/error');
    }
});

Route::filter('users_manage', function()
{
    if (! Entrust::can('users_manage') ) // Checks the current user
    {
        return Redirect::to('/error');
    }
});

Route::filter('message_send', function()
{
    if (! Entrust::can('message_send') ) // Checks the current user
    {
        return Redirect::to('/error');
    }
});

Route::filter('ads_manage', function()
{
    if (! Entrust::can('ads_manage') ) // Checks the current user
    {
        return Redirect::to('/error');
    }
});

Route::filter('edition_manage', function()
{
    if (! Entrust::can('edition_manage') ) // Checks the current user
    {
        return Redirect::to('/error');
    }
});

Route::filter('mistake_manage', function()
{
    if (! Entrust::can('mistake_manage') ) // Checks the current user
    {
        return Redirect::to('/error');
    }
});

Route::filter('number_manage', function()
{
    if (! Entrust::can('number_manage') ) // Checks the current user
    {
        return Redirect::to('/error');
    }
});

Route::filter('export_txt', function()
{
    if (! Entrust::can('export_txt') ) // Checks the current user
    {
        return Redirect::to('/error');
    }
});

Route::filter('cms_manage', function()
{
    if (! Entrust::can('cms_manage') ) // Checks the current user
    {
        return Redirect::to('/error');
    }
});