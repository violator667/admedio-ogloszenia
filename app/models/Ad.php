<?php

class Ad extends \Eloquent {
	protected $fillable = [];

    public function editors()
    {
        return $this->hasMany('Editor', 'ad_id', 'id')->orderBy('id', 'DESC')->take(5);
    }

    public function category()
    {
        return $this->hasOne('Category', 'id', 'category_export');
    }

    public function internalCategory()
    {
        return $this->hasOne('InternalCategory', 'id', 'internal_category_id');
    }

    public function edition()
    {
        return $this->hasOne('Edition', 'id', 'last_edition_id');
    }

    public function delete()
    {
        $this->editors()->delete();
        $exported = $this->exported_id;
        parent::delete();
        //delete external
        try {

            $external = ExportRecord::findOrFail($exported);
            $external->delete();

        }catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {

        }

    }

    public function save(array $options = array())
    {
        if($this->starting_at == "0000-00-00 00:00:00" )
        {
            if($this->active == 1) {
                $this->starting_at = \Carbon\Carbon::now();
            }
        }
        parent::save($options);
    }
}