<?php

class Category extends \Eloquent {
//	protected $fillable = ['name'];
//    protected $table = 'category';

    protected $table = 'wt6id_djcf_categories';
    protected $connection = 'mysql-export-to';

    public function parent()
    {
        return $this->hasOne('Category', 'id', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('Category', 'parent_id', 'id');
    }

    public function tree()
    {
        return static::with(implode('.', array_fill(0,10, 'children')))->where('parent_id', '=', '0')->get();
    }


}