<?php

class CategoryHelper {

    private $items;

    public function __construct($items = NULL) {
        $this->items = $items;
    }

    public function htmlList() {
        return $this->htmlFromArray($this->itemArray());
    }

    public function htmlSelect() {
//        echo "<pre>";
//        print_r($this->itemArray());
//        echo "</pre>";
        return $this->htmlOptions($this->itemArray());
    }

    public function htmlOptions($array, $spacer = NULL)
    {

        $html = '';
        foreach($array as $k=>$v) {

            $html .= '<option value="'.$v['id'].'">'.$spacer.' '.$v['name'].'</option>';
            if(count($v['children']) > 0) {
                $spacer = $spacer.''.$v['name'].' > ';
                $html .= $this->htmlOptions($v['children'], $spacer);
                $spacer = '';
            }

        }
        return $html;
    }

    private function htmlFromArray($array) {
        $html = '';
        foreach($array as $k=>$v) {
            $html .= "<ul>";
            $html .= "<li>".$k."</li>";
            if(count($v) > 0) {
                $html .= $this->htmlFromArray($v);
            }
            $html .= "</ul>";
        }
        return $html;
    }

    private function itemArray() {
        $result = array();
        foreach($this->items as $item) {
            if ($item->parent_id == 0) {
                $result[] = array('id' => $item->id, 'name' => $item->name, 'children' => $this->itemWithChildren($item));
            }
        }
        return $result;
    }

    private function itemWithChildren($item) {
        $result = array();
        $children = $this->childrenOf($item);
        foreach ($children as $child) {
            //$result[$child->name] = $this->itemWithChildren($child);
            $result[] = array('id' => $child->id, 'name' => $child->name, 'children' => $this->itemWithChildren($child));
        }
        return $result;
    }

    private function itemArray2() {
        $result = array();
        foreach($this->items as $item) {
            if ($item->parent_id == 0) {
                $result[$item->name] = $this->itemWithChildren($item);
            }
        }
        return $result;
    }

    private function childrenOf($item) {
        $result = array();
        foreach($this->items as $i) {
            if ($i->parent_id == $item->id) {
//                $result[] = array('id' => $item->id, 'name' => $item->name);
                $result[] = $i;
            }
        }
        return $result;
    }

    private function childrenOf2($item) {
        $result = array();
        foreach($this->items as $i) {
            if ($i->parent_id == $item->id) {
                $result[] = $i;
            }
        }
        return $result;
    }

    private function itemWithChildren2($item) {
        $result = array();
        $children = $this->childrenOf($item);
        foreach ($children as $child) {
            $result[$child->name] = $this->itemWithChildren($child);
        }
        return $result;
    }

    public function getAllParents($item_id)
    {
        $parents_array = array();
        $p = $this->getParent($item_id);
        if(is_array($p)) {
            $parents_array[] = $p;
            $pp = $this->getParent($p['id']);
            if(is_array($pp)) {
                $parents_array[] = $pp;
                $ppp = $this->getParent($pp['id']);
                if(is_array($ppp)) {
                    $parents_array[] = $ppp;
                    $pppp = $this->getParent($ppp['id']);
                    if(is_array($pppp)) {
                        $parents_array[] = $pppp;
                        $ppppp = $this->getParent($pppp['id']);
                    }
                }
            }
            return $parents_array;
        }else{
            return array();
        }
    }

    public function getParent($item_id)
    {
        if($item_id != 0) {
            $cat = Category::find($item_id);
            if($cat->parent_id!=0) {
                $parent_obj = Category::find($cat->parent_id);
                $parent = array('name' => $parent_obj->name, 'id' => $parent_obj->id);
                return $parent;
            }
        }

    }

}