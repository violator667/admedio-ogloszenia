<?php

class Edition extends \Eloquent {
	protected $fillable = ['name','start','end'];
}