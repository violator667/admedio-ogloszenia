<?php

class ExportRecord extends \Eloquent {
    protected $fillable = [];
    protected $table = 'wt6id_djcf_items';
    protected $connection = 'mysql-export-to';
    public $timestamps = false;
}