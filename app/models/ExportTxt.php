<?php

class ExportTxt extends \Eloquent {
	protected $fillable = ['finished','start','end','finished_at'];
    protected $table = 'export';

    public function user()
    {
        return $this->belongsTo('User', 'user_id', 'id');
    }
}