<?php

class ExternalImage extends \Eloquent {
	protected $fillable = ['item_id', 'type', 'name', 'ext', 'path', 'caption', 'ordering'];
    protected $table = 'wt6id_djcf_images';
    protected $connection = 'mysql-export-to';
    public $timestamps = false;
}