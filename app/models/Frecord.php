<?php

class Frecord extends \Eloquent {
    //Export FROM
	protected $fillable = [];
    protected $table = 'iy4l9_facileforms_records';
    protected $connection = 'mysql-export-from';

    public function subrecords()
    {
        return $this->hasMany('Fsubrecord', 'record', 'id');
    }
}