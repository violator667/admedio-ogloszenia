<?php

class Fsubrecord extends \Eloquent {
    //Export FROM
	protected $fillable = [];
    protected $table = 'iy4l9_facileforms_subrecords';
    protected $connection = 'mysql-export-from';

    public function record()
    {
        return $this->belongsTo('Frecord', 'id');
    }
}