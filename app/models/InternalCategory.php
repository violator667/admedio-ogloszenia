<?php

class InternalCategory extends \Eloquent {
	protected $fillable = ['name'];
    protected $table = 'category';

}