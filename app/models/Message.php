<?php

class Message extends \Eloquent {
	protected $fillable = [];


    public function scopeUnread($query)
    {
        return $query->where('read', '==', 0);
    }

    public function scopeRead($query)
    {
        return $query->where('read', '==', 1);
    }

}