<?php

class Page extends \Eloquent {
	protected $fillable = ['title', 'body'];
}