<?php

use Zizaco\Confide\ConfideUser;
use Zizaco\Confide\ConfideUserInterface;

class User extends Eloquent implements ConfideUserInterface
{
    use ConfideUser, \Zizaco\Entrust\HasRole;

    protected $fillable = ['password'];

//    public function roles()
//    {
//        return $this->belongsToMany('Role','assigned_roles');
//    }

    public function messages()
    {
        //recieved messages
        return $this->hasMany('Message','reciever_id');
    }


    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
        $this->save();
    }

    public function delete()
    {
        $this->roles()->detach();
        //deletes recieved messages
        $this->messages()->delete();
        //deletes sent messages
        Message::where('user_id', $this->id)->delete();
        parent::delete();
    }

}