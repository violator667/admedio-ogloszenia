<?php
Config::set('laravel-debugbar::config.enabled', false);
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
//	return View::make('hello');
    return Redirect::to('/panel');
});
Route::get('/error', function()
{
	return View::make('error');
});

Route::group(array('before' => 'auth|panel_access'), function() {
    Route::get('panel', 'PanelController@index');

//    Route::get('panel/category/{parent_id?}', array('before' => 'category_manage', 'uses' => 'CategoryController@index'))->where('parent_id', '[0-9]+');
    Route::get('panel/category/{parent_id?}', array('uses' => 'CategoryController@index'))->where('parent_id', '[0-9]+');
    Route::get('panel/category/json/{parent_id}', array('before' => 'ads_manage', 'uses' => 'CategoryController@catjson'))->where('parent_id', '[0-9]+');

//    Route::get('panel/internal-category', array('before' => 'category_manage', 'uses' => 'InternalCategoryController@index'));
    Route::get('panel/internal-category', array('uses' => 'InternalCategoryController@index'));
    Route::get('panel/internal-category/add', array('before' => 'category_manage', 'uses' => 'InternalCategoryController@add'));
    Route::post('panel/internal-category/add', array('before' => 'category_manage', 'uses' => 'InternalCategoryController@create'));
    Route::get('panel/internal-category/{id}/edit', array('before' => 'category_manage', 'uses' => 'InternalCategoryController@edit', 'as' => 'internal_category_update'))->where('id', '[0-9]+');
    Route::post('panel/internal-category/{id}/edit', array('before' => 'category_manage', 'uses' => 'InternalCategoryController@update'))->where('id', '[0-9]+');
    Route::post('panel/internal-category/delete', array('before' => 'category_manage', 'uses' => 'InternalCategoryController@delete'));

    Route::get('panel/users/', array('before' => 'users_manage', 'uses' => 'UsersController@index'));
    Route::get('panel/users/add', array('before' => 'users_manage', 'uses' => 'UsersController@add'));
    Route::post('panel/users/add', array('before' => 'users_manage|csrf', 'uses' => 'UsersController@admin_create'));
    Route::get('panel/users/{id}/edit', array('before' => 'users_manage', 'uses' => 'UsersController@edit', 'as' => 'user_update'))->where('id', '[0-9]+');
    Route::post('panel/users/{id}/edit', array('before' => 'users_manage|csrf', 'uses' => 'UsersController@update'))->where('id', '[0-9]+');
    Route::post('panel/users/delete', array('before' => 'users_manage|csrf', 'uses' => 'UsersController@delete'));
    Route::get('panel/users/{id}/delete', array('before' => 'users_manage', 'uses' => 'UsersController@delete'))->where('id', '[0-9]+');

    Route::get('panel/messages', array('uses' => 'MessageController@index'));
    Route::get('panel/messages/show/{id}', array('uses' => 'MessageController@show'))->where('id', '[0-9]+');
    Route::get('panel/messages/{id}/delete', array('uses' => 'MessageController@delete'))->where('id', '[0-9]+');
    Route::get('panel/messages/send-to-user-id/{id?}', array('before' => 'message_send', 'uses' => 'MessageController@add'))->where('id', '[0-9]+');
    Route::post('panel/messages/send-to-user-id', array('before|csrf' => 'message_send|csrf', 'uses' => 'MessageController@send'));

    Route::get('panel/ads/import/{number?}', array('before' => 'ads_manage', 'uses' => 'ImportController@import'))->where('number', '[0-9]+');
    Route::get('panel/ads/add', array('before' => 'ads_manage', 'uses' => 'AdController@create'));
    Route::post('panel/ads/add', array('before' => 'ads_manage|csrf', 'uses' => 'AdController@store'));

    Route::get('panel/ads/search/reset', array('before' => 'ads_manage', 'uses' => 'AdController@search_reset'));
    Route::post('panel/ads/search/{option?}', array('before' => 'ads_manage|csrf', 'uses' => 'AdController@search'))->where('option', '[a-zA-Z]+');
    Route::post('panel/ads/refresh', array('before' => 'ads_manage|csrf', 'uses' => 'AdController@refresh'));
    Route::post('panel/ads/delete', array('before' => 'ads_manage|csrf', 'uses' => 'AdController@massdelete'));
    Route::get('panel/ads/export/txt', array('before' => 'export_txt', 'uses' => 'AdController@export_txt'));
    Route::post('panel/ads/export/txt', array('before' => 'export_txt|csrf', 'uses' => 'AdController@export_txt_post'));
    Route::get('panel/ads/export/getfile/{filename}', array('before' => 'export_txt', 'uses' => 'AdController@getfile'))->where('filename','[0-9]+');
    Route::any('panel/ads/{option?}', array('before' => 'ads_manage', 'uses' => 'AdController@index'))->where('option', '[a-zA-Z]+');
    Route::any('panel/ads/{id}/unlock/{redir?}/{path?}', array('before' => 'ads_manage', 'uses' => 'AdController@unlock'))->where(['id' => '[0-9]+', 'redir' => '[a-z]+', 'path' => '[a-z]+']);
    Route::get('panel/ads/{id}/edit', array('before' => 'ads_manage', 'uses' => 'AdController@edit'))->where('id', '[0-9]+');
    Route::post('panel/ads/{id}/edit', array('before' => 'ads_manage', 'uses' => 'AdController@update'))->where('id', '[0-9]+');
    Route::get('panel/ads/{id}/delete', array('before' => 'ads_manage', 'uses' => 'AdController@destroy'))->where('id', '[0-9]+');

    Route::get('panel/mistake/{user_id}/{ad_id}/', array('before' => 'mistake_manage', 'uses' => 'UsersController@mistake'))->where(array('user_id' => '[0-9]+', 'ad_id' => '[0-9]+'));

    Route::get('panel/edition', array('uses' => 'EditionController@index'));
//    Route::get('panel/edition', array('before' => 'edition_manage', 'uses' => 'EditionController@index'));
    Route::get('panel/edition/add', array('before' => 'edition_manage', 'uses' => 'EditionController@create'));
    Route::post('panel/edition/add', array('before' => 'edition_manage|csrf', 'uses' => 'EditionController@store'));
    Route::get('panel/edition/{id}/edit', array('before' => 'edition_manage', 'uses' => 'EditionController@edit', 'as' => 'edition_update'))->where('id', '[0-9]+');
    Route::post('panel/edition/{id}/edit', array('before' => 'edition_manage|csrf', 'uses' => 'EditionController@update'))->where('id', '[0-9]+');

//    Route::get('panel/numbers', array('before' => 'number_manage', 'uses' => 'NumberController@index'));
    Route::get('panel/numbers', array('uses' => 'NumberController@index'));
    Route::get('panel/numbers/add', array('before' => 'number_manage', 'uses' => 'NumberController@create'));
    Route::post('panel/numbers/add', array('before' => 'number_manage|csrf', 'uses' => 'NumberController@store'));
    Route::get('panel/numbers/{id}/delete', array('before' => 'number_manage', 'uses' => 'NumberController@destroy'))->where('id', '[0-9]+');
    Route::get('panel/numbers/{id}/edit', array('before' => 'number_manage', 'uses' => 'NumberController@edit'))->where('id', '[0-9]+');
    Route::post('panel/numbers/{id}/edit', array('before' => 'number_manage|csrf', 'uses' => 'NumberController@update'))->where('id', '[0-9]+');

    Route::get('panel/cms', array('uses' => 'PageController@index'));
    Route::get('panel/cms/add', array('before' => 'cms_manage', 'uses' => 'PageController@create'));
    Route::post('panel/cms/add', array('before' => 'cms_manage|csrf', 'uses' => 'PageController@store'));
    Route::get('panel/cms/{id}', array('uses' => 'PageController@show'))->where('id', '[0-9]+');
    Route::get('panel/cms/{id}/edit', array('before' => 'cms_manage', 'uses' => 'PageController@edit'))->where('id', '[0-9]+');
    Route::post('panel/cms/{id}/edit', array('before' => 'cms_manage', 'uses' => 'PageController@update'))->where('id', '[0-9]+');
    Route::get('panel/cms/{id}/delete', array('before' => 'cms_manage', 'uses' => 'PageController@destroy'))->where('id', '[0-9]+');
});

//test routes

Route::get('tmp/art', function() {

});

//TMP routes

//make roles in DB


Route::get('tmp/roles', function()
{
    $administrator = new Role;
    $administrator->name = 'Administrator';
    $administrator->save();

    $biuro = new Role;
    $biuro->name = 'Biuro';
    $biuro->save();

    $biuro_vip = new Role;
    $biuro_vip->name = 'Biuro VIP';
    $biuro_vip->save();

    $vip = new Role;
    $vip->name = 'VIP';
    $vip->save();

    $korekta = new Role;
    $korekta->name = 'Korekta';
    $korekta->save();
});

Route::get('tmp/assignroles', function() {
    $user = User::where('email','=','programista.michal@ernes.pl')->first();
    $administrator = Role::where('name','=','Administrator')->first();
    $user->attachRole( $administrator );
});

Route::get('tmp/addpermissions', function() {

    # PANEL ACCESS
    $panel_access = new Permission;
    $panel_access->name = 'panel_access';
    $panel_access->display_name = 'Dostęp do panelu.';
    $panel_access->save();

    #ADS MANAGE
    $ads_manage = new Permission;
    $ads_manage->name = 'ads_manage';
    $ads_manage->display_name = 'Dodawanie, edytowanie, usuwanie ogłoszeń.';
    $ads_manage->save();

    #EXPORT TXT
    $export_txt = new Permission;
    $export_txt->name = 'export_txt';
    $export_txt->display_name = 'Eksport do pliku tekstowego.';
    $export_txt->save();

    #NUMBER MANAGE
    $number_manage = new Permission;
    $number_manage->name = 'number_manage';
    $number_manage->display_name = 'Dodawanie/usuwanie numerów zastrzeżonych z listy.';
    $number_manage->save();

    #MISTAKE MANAGE
    $mistake_manage = new Permission;
    $mistake_manage->name = 'mistake_manage';
    $mistake_manage->display_name = 'Przydzielanie błędów użytkownikom.';
    $mistake_manage->save();

    #CMS MANAGE
    $cms_manage = new Permission;
    $cms_manage->name = 'cms_manage';
    $cms_manage->display_name = 'Edytowanie i dodawanie nowych podstron do systemu (cms).';
    $cms_manage->save();

    #MESSAGE SEND
    $message_send = new Permission;
    $message_send->name = 'message_send';
    $message_send->display_name = 'Możliwość wysyłania komunikatów od użytkowników.';
    $message_send->save();

    #EDITION MANAGE
    $edition_manage = new Permission;
    $edition_manage->name = 'edition_manage';
    $edition_manage->display_name = 'Edytowanie i dodawanie numerów wydań (numer wydania oraz czas jego trwania od - do).';
    $edition_manage->save();

    #USERS MANAGE
    $users_manage = new Permission;
    $users_manage->name = 'users_manage';
    $users_manage->display_name = 'Dodawanie/usuwanie/edycja użytkowników, przypisywanie do grup.';
    $users_manage->save();

    #CATEGORY MANAGE
    $category_manage = new Permission;
    $category_manage->name = 'category_manage';
    $category_manage->display_name = 'Dodawanie/usuwanie/edycja kategorii.';
    $category_manage->save();


    #SYNC
    $administrator = Role::where('name','=','Administrator')->first();
    $administrator->perms()->sync(array($panel_access->id,
                                        $ads_manage->id,
                                        $export_txt->id,
                                        $number_manage->id,
                                        $mistake_manage->id,
                                        $cms_manage->id,
                                        $message_send->id,
                                        $edition_manage->id,
                                        $users_manage->id,
                                        $category_manage->id));

    $biuro = Role::where('name','=','Biuro')->first();
    $biuro->perms()->sync(array($panel_access->id, $ads_manage->id));

    $biuro_vip = Role::where('name','=','Biuro VIP')->first();
    $biuro_vip->perms()->sync(array($panel_access->id,
                                    $ads_manage->id,
                                    $number_manage->id,
                                    $mistake_manage->id,
                                    $cms_manage->id,
                                    $message_send->id,
                                    $edition_manage->id));

    $vip = Role::where('name','=','VIP')->first();
    $vip->perms()->sync(array($panel_access->id,
                                $ads_manage->id,
                                $message_send->id));

    $korekta = Role::where('name','=','Korekta')->first();
    $korekta->perms()->sync(array($panel_access->id,
                                    $ads_manage->id,
                                    $export_txt->id,
                                    $number_manage->id,
                                    $mistake_manage->id));

});


// Confide routes
//Route::get('users/create', 'UsersController@create');
//Route::post('users', 'UsersController@store');
Route::get('users/login', 'UsersController@login');
Route::post('users/login', 'UsersController@doLogin');
//Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('users/logout', 'UsersController@logout');

//IMPORTANT ! - make some messages notices
if(Auth::id()) {
    $messages = User::with('messages')->find(Auth::id());
    $messages_number = $messages->messages()->unread()->count();

    try {
        $current_edition = Edition::where('start', '<=', \Carbon\Carbon::now())->where('end', '>', \Carbon\Carbon::now())->firstOrFail();
        View::share('current_edition', $current_edition);
    } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

        $current_edition = Edition::find(1);
        View::share('current_edition', $current_edition);
    }


    View::share('messages_number', $messages_number);
    if($messages_number > 0) {
        $messages_unread = $messages->messages()->unread()->take(5)->get();
        View::share('messages_unread', $messages_unread);
    }else{
        View::share('messages_unread', []);
    }

}
