@include('partial.header')
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="index.html">
                <img src="{{{ Config::get('app.url') }}}/assets/admin/layout/img/logo.png" alt="logo" class="logo-default"/>
            </a>
            <div class="menu-toggler sidebar-toggler hide">
            </div>
        </div>
        <!-- END LOGO -->
        @include('partial.topmenu')
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    @include('partial.menu')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="/panel">Panel główny</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="/panel/ads">Ogłoszenia</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="/panel/ads/add">Dodaj ogłoszenie</a>
                    </li>
                </ul>
                <div class="page-toolbar">

                </div>

            </div>



            @if( Session::has('notice') )
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <strong>Info!</strong> {{{ Session::get('notice') }}}
                </div>
                @endif
            @if(!$errors->isEmpty())
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <strong>Błąd!</strong>
                    @foreach( $errors->all() as $error)
                        <li> {{{ $error }}}</li>
                    @endforeach
                </div>
                @endif
                        <!-- END PAGE HEADER-->
                <!-- PAGE CONTRNTENT -->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-slack"></i>Dodaj ogłoszenie
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title="">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <form action="/panel/ads/add" class="form-horizontal" method="post">
                        {{ Form::token(); }}
                        <input type="hidden" name="category_export" class="form-control" id="category_export" value="{{{ Input::old('category_export') }}}">
                        <input type="hidden" name="actual_edition_id" class="form-control" value="{{{ $edition->id }}}">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Treść ogłoszenia:</label>
                                    <div class="col-md-4">
                                        <textarea name="ad_text" class="form-control autosizeme" rows="6" id="ad_text" maxlength="500">{{{ Input::old('ad_text') }}}</textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <span class="text-muted">[ wymagane ]</span>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Cena:</label>
                                    <div class="col-md-4">
                                        <input type="text" name="price" class="form-control" value="{{{ Input::old('price') }}}">
                                    </div>
                                    <div class="col-md-4">
                                        <span class="text-muted"></span>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tel. 1:</label>
                                    <div class="col-md-4">
                                        <input type="text" name="phone_1" class="form-control" value="{{{ Input::old('phone_1') }}}">
                                    </div>
                                    <div class="col-md-4">
                                        <span class="text-muted"></span>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tel. 2:</label>
                                    <div class="col-md-4">
                                        <input type="text" name="phone_2" class="form-control" value="{{{ Input::old('phone_2') }}}">
                                    </div>
                                    <div class="col-md-4">
                                        <span class="text-muted"></span>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>
                                <div class="form-group btn-show-btn">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-4">
                                        <input type="button" class="btn green-meadow" value="Pokaż wszystko" id="btn-show-all">
                                    </div>
                                    <div class="col-md-4"></div>
                                </div>
                                <div class="form-group btn-show">
                                    <label class="col-md-3 control-label">Tel. zagran:</label>
                                    <div class="col-md-4">
                                        <input type="text" name="phone_3" class="form-control" value="{{{ Input::old('phone_3') }}}">
                                    </div>
                                    <div class="col-md-4">
                                        <span class="text-muted"></span>
                                    </div>
                                </div>
                                <div class="clearfix btn-show">
                                </div>
                                <div class="form-group btn-show">
                                    <label class="col-md-3 control-label">Email kontaktowy:</label>
                                    <div class="col-md-4">
                                        <input type="text" name="contact_email" class="form-control" value="{{{ Input::old('contact_email') }}}">
                                    </div>
                                    <div class="col-md-4">
                                        <span class="text-muted"></span>
                                    </div>
                                </div>
                                <div class="clearfix btn-show">
                                </div>
                                <div class="form-group btn-show">
                                    <label class="col-md-3 control-label">Miejsce/godzina:</label>
                                    <div class="col-md-4">
                                        <input type="text" name="place" class="form-control" value="{{{ Input::old('place') }}}">
                                    </div>
                                    <div class="col-md-4">
                                        <span class="text-muted"></span>
                                    </div>
                                </div>
                                <div class="clearfix btn-show">
                                </div>
                                <div class="form-group btn-show">
                                    <label class="col-md-3 control-label">Wyeksportuj:</label>
                                    <div class="col-md-4">
                                       <select name="can_export" class="form-control">
                                           @if(Input::old('can_export'))
                                               @if(Input::get('can_export') == "yes")
                                                   <option value="yes" selected>TAK</option>
                                                   <option value="no">NIE</option>
                                               @else
                                                   <option value="yes">TAK</option>
                                                   <option value="no" selected>NIE</option>
                                               @endif
                                           @else
                                               <option value="yes" selected>TAK</option>
                                               <option value="no">NIE</option>
                                           @endif
                                       </select>

                                    </div>
                                    <div class="col-md-4">
                                        <span class="text-muted"></span>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>

                                <hr>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Zakończenie emisji:</label>
                                    {{--<div class="col-md-4">--}}
                                        {{--@if(Input::old('editions_number'))--}}
                                            {{--<input type="text" name="editions_number" class="form-control" value="{{{ Input::old('editions_number') }}}">--}}
                                        {{--@else--}}
                                            {{--<input type="text" name="editions_number" class="form-control" value="1">--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                    <div class="col-md-4">
                                        <select name="last_edition_id" class="form-control">
                                            {{ $i = 1 }}
                                            @if(Input::old('last_edition_id'))
                                                @foreach($all_editions as $edition1)
                                                    @if($edition1->id == Input::old('last_edition_id'))
                                                        <option value="{{{ $edition1->id }}}" selected>{{{ $i }}} - [{{{ $edition1->name }}}] do {{{ $edition1->end }}}</option>
                                                    @else
                                                        <option value="{{{ $edition1->id }}}">{{{ $i }}} - [{{{ $edition1->name }}}] do {{{ $edition1->end }}}</option>
                                                    @endif
                                                    {{ $i++ }}
                                                @endforeach
                                            @else
                                                <option value="0">[ brak ]</option>
                                                @foreach($all_editions as $edition1)
                                                 <option value="{{{ $edition1->id }}}">{{{ $i }}} - [{{{ $edition1->name }}}] do {{{ $edition1->end }}}</option>
                                                {{ $i++ }}
                                                @endforeach
                                            @endif
                                        </select>

                                    </div>
                                    <div class="col-md-4">
                                        <span class="text-muted">[ aktualne wydanie: [{{{ $edition->name }}}] do {{{ $edition->end }}}]</span>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Kategoria wewnętrzna:</label>
                                    <div class="col-md-4">
                                        <select name="internal_category_id" class="form-control">

                                                <option value="0" selected>[ brak ]</option>

                                            @foreach($internal_categories as $int_cat)
                                                <option value="{{{ $int_cat->id }}}">{{{ $int_cat->name }}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                    <div class="col-md-4">
                                        <span class="text-muted">[wymagane]</span>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Wybrana kategoria (e-dział):</label>
                                    <div class="col-md-4">
                                        <span class="cat_1">
                                            @if(isset($old_cat[0]))
                                                {{{ $old_cat[0]['name'] }}}
                                            @else
                                                [ brak ]
                                            @endif
                                        </span>
                                        <span class="cat_2">
                                            @if(isset($old_cat[1]))
                                                > {{{ $old_cat[1]['name'] }}}
                                            @endif
                                        </span>
                                        <span class="cat_3">
                                            @if(isset($old_cat[2]))
                                                > {{{ $old_cat[2]['name'] }}}
                                            @endif
                                        </span>
                                        <span class="cat_4">
                                            @if(isset($old_cat[3]))
                                                > {{{ $old_cat[3]['name'] }}}
                                            @endif
                                        </span>
                                        <span class="cat_5">
                                            @if(isset($old_cat[4]))
                                                > {{{ $old_cat[4]['name'] }}}
                                            @endif
                                        </span>
                                        <span class="cat_id">
                                            @if(Input::old('category_export'))
                                                [{{{ Input::old('category_export') }}}]
                                            @endif
                                            </span>
                                    </div>
                                    <div class="col-md-4">
                                        <span class="text-muted"></span>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Wybierz kategorię (e-dział):</label>
                                    <div class="col-md-2">
                                        <select size="5" class="form-control" id="category_1">
                                            @foreach($main_categories as $main_category)
                                                <option value="{{{ $main_category->id }}}">{{{ $main_category->name }}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <select size="5" class="form-control" id="category_2">

                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <select size="5" class="form-control" id="category_3">

                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <select size="5" class="form-control" id="category_4">

                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <select size="5" class="form-control" id="category_5">

                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Notatka:</label>
                                    <div class="col-md-4">
                                        <textarea name="note" class="form-control">{{{ Input::old('note') }}}</textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <span class="text-muted"></span>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"></label>
                                    <div class="col-md-4">
                                        <input type="submit" class="btn green-meadow" value="Dodaj ogłoszenie"></form>
                                    </div>
                                    <div class="col-md-4">
                                        <span class="text-muted"></span>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <!-- BEGIN PORTLET-->

                        <!-- END PORTLET-->
                    </div>
                    <div class="col-md-6 col-sm-6" style="text-align: right;">
                        <!-- BEGIN PORTLET-->

                        <!-- END PORTLET-->
                    </div>
                </div>
                <div class="clearfix">
                </div>
                <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        @include('partial.footer')
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/respond.min.js"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{{ Config::get('app.url') }}}/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/ui-confirmations.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/table-managed.js"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
        Index.init();
        Index.initDashboardDaterange();
        Index.initJQVMAP(); // init index page's custom scripts
        Index.initCalendar(); // init index page's custom scripts
        Index.initCharts(); // init index page's custom scripts
        Index.initChat();
        Index.initMiniCharts();
        Tasks.initDashboardWidget();

        $('#category_1').change(function() {
            var value = $('#category_1 option:selected').val();
            var text  = $('#category_1 option:selected').text();
            $('#category_export').val(value);
            $('.cat_id').html('['+value+']');
            $('.cat_1').html(text);
            $('.cat_2').html('');
            $('.cat_3').html('');
            $('.cat_4').html('');
            $('.cat_5').html('');
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });

            $('#category_2').find('option').remove().end();
            $('#category_3').find('option').remove().end();
            $('#category_4').find('option').remove().end();
            $('#category_5').find('option').remove().end();

            $.getJSON( "/panel/category/json/"+ value, function( data ) {

                var items = [];
//                console.log(data);
                $.each( data, function( key, val ) {
                    items.push( val );
                });
//                console.log(items);
                $.each( items, function( key, val ) {
                    console.log(val)
                    $('#category_2').append($('<option>', {
                        value: val.id,
                        text: val.name
                    }));
                });
            });
        });

        $('#category_2').change(function() {
            var value = $('#category_2 option:selected').val();
            var text  = $('#category_2 option:selected').text();
            $('#category_export').val(value);
            $('.cat_id').html('['+value+']');
            $('.cat_2').html(' > ' + text);
            $('.cat_3').html('');
            $('.cat_4').html('');
            $('.cat_5').html('');
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });
            $('#category_3').find('option').remove().end();
            $('#category_4').find('option').remove().end();
            $('#category_5').find('option').remove().end();
            $.getJSON( "/panel/category/json/"+ value, function( data ) {

                var items = [];
//                console.log(data);
                $.each( data, function( key, val ) {
                    items.push( val );
                });
//                console.log(items);
                $.each( items, function( key, val ) {
                    console.log(val)
                    $('#category_3').append($('<option>', {
                        value: val.id,
                        text: val.name
                    }));
                });
            });
        });

        $('#category_3').change(function() {
            var value = $('#category_3 option:selected').val();
            var text  = $('#category_3 option:selected').text();
            $('#category_export').val(value);
            $('.cat_id').html('['+value+']');
            $('.cat_3').html(' > ' + text);
            $('.cat_4').html('');
            $('.cat_5').html('');
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });
            $('#category_4').find('option').remove().end();
            $('#category_5').find('option').remove().end();
            $.getJSON( "/panel/category/json/"+ value, function( data ) {

                var items = [];
//                console.log(data);
                $.each( data, function( key, val ) {
                    items.push( val );
                });
//                console.log(items);
                $.each( items, function( key, val ) {
                    console.log(val)
                    $('#category_4').append($('<option>', {
                        value: val.id,
                        text: val.name
                    }));
                });
            });
        });

        $('#category_4').change(function() {
            var value = $('#category_4 option:selected').val();
            var text  = $('#category_4 option:selected').text();
            $('#category_export').val(value);
            $('.cat_id').html('['+value+']');
            $('.cat_4').html(' > ' + text);
            $('.cat_5').html('');
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });
            $('#category_5').find('option').remove().end();
            $.getJSON( "/panel/category/json/"+ value, function( data ) {

                var items = [];
//                console.log(data);
                $.each( data, function( key, val ) {
                    items.push( val );
                });
//                console.log(items);
                $.each( items, function( key, val ) {
                    console.log(val)
                    $('#category_5').append($('<option>', {
                        value: val.id,
                        text: val.name
                    }));
                });
            });
        });

        $('#category_5').change(function() {
            var value = $('#category_5 option:selected').val();
            var text  = $('#category_5 option:selected').text();
            $('#category_export').val(value);
            $('.cat_id').html('['+value+']');
            $('.cat_5').html(' > ' + text);

            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });

        });

        $('#ad_text').maxlength({
            alwaysShow: true,
            warningClass: "label label-success",
            limitReachedClass: "label label-danger",
            separator: ' z ',
            preText: '',
            postText: ' dostępnych znaków (max 300 - 13b, 19)',
            placement: 'top',
            validate: true
        });

        $('.btn-show').hide();
        $('#btn-show-all').click(function( event ) {
            event.preventDefault();
            $('.btn-show').fadeIn();
            $('#btn-show-all').fadeOut();
        });

    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>