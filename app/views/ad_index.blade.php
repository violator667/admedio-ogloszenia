@include('partial.header')
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="index.html">
                <img src="{{{ Config::get('app.url') }}}/assets/admin/layout/img/logo.png" alt="logo" class="logo-default"/>
            </a>
            <div class="menu-toggler sidebar-toggler hide">
            </div>
        </div>
        <!-- END LOGO -->
        @include('partial.topmenu')
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    @include('partial.menu')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="/panel">Panel główny</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="/panel/ads">Ogłoszenia</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="/panel/ads/{{{ $submenu['link'] }}}">{{{ $submenu['text'] }}}</a>
                    </li>
                </ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="true">
                            Opcje <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <a href="/panel/ads/add"><i class="fa  fa-plus"></i>
                                    Dodaj</a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="#delete" data-toggle="modal" id="ads_delete"><i class="fa fa-trash-o"></i> Usuń</a>
                            </li>
                            <li>
                                <a href="#refresh" data-toggle="modal" id="ads_refresh"><i class="fa fa-refresh"></i> Ponów</a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="/panel/ads/import"><i class="fa fa-cloud-download"></i> Importuj</a>
                            </li>
                            {{--<li class="divider">--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a id="ads_export_txt" href="/panel/ads/export/txt"><i class="fa fa-cloud-upload"></i> Eksportuj do TXT</a>--}}
                            {{--</li>--}}
                        </ul>
                    </div>
                </div>

            </div>



            @if( Session::has('notice') )
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <strong>Info!</strong> {{ Session::get('notice') }}
                </div>
                @endif
            @if(!$errors->isEmpty())
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <strong>Błąd!</strong>
                    @foreach( $errors->all() as $error)
                        <li> {{{ $error }}}</li>
                    @endforeach
                </div>
                @endif
                        <!-- END PAGE HEADER-->
                <!-- PAGE CONTRNTENT -->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-search"></i>Wyszukaj
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title="">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <form action="/panel/ads/search" method="post">
                                {{ Form::token() }}
                                <div class="col-md-2">
                                    <select name="search_user" class="form-control">
                                        <option value="0">[ wszyscy użytkownicy ]</option>
                                        @foreach( $users_list as $user)
                                            @if($user->id == Session::get('search_user'))
                                                <option value="{{{ $user->id }}}" selected>{{{ $user->name }}}</option>
                                            @else
                                                <option value="{{{ $user->id }}}">{{{ $user->name }}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <select name="search_category" style="width: 165px !important;" class="form-control">
                                        <option value="0">[ wszystkie kategorie ]</option>
                                        @foreach( $category_list as $category)
                                            @if($category->id == Session::get('search_category'))
                                                <option value="{{{ $category->id }}}" selected>{{{ $category->name }}}</option>
                                            @else
                                                <option value="{{{ $category->id }}}">{{{ $category->name }}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2"><input type="text" name="search_text" placeholder="szukana fraza" value="{{{ Session::get('search_text') }}}" class="form-control"></div>
                                <div class="col-md-3">
                                    {{--<div class="input-group input-small date form_datetimeaaa" data-date="{{{ date('Y-m-d') }}}" data-date-format="yyyy-mm-dd" data-date-viewmode="years">--}}
                                        {{--<input type="text" class="form-control" readonly="" name="search_start" value="{{{ Session::get('search_start') }}}">--}}
												{{--<span class="input-group-btn">--}}
												{{--<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>--}}
												{{--</span>--}}
                                    {{--</div>--}}

                                    <div class="input-group date form_datetime">
                                        <input type="text" class="form-control" readonly="" name="search_start" value="{{{ Session::get('search_start') }}}">
												<span class="input-group-btn">
												<button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
												</span>
                                    </div>

                                </div>
                                <div class="col-md-3">
                                    {{--<div class="input-group input-small date date-picker" data-date="{{{ date('Y-m-d') }}}" data-date-format="yyyy-mm-dd" data-date-viewmode="years">--}}
                                        {{--<input type="text" class="form-control" readonly="" name="search_end" value="{{{ Session::get('search_end') }}}">--}}
												{{--<span class="input-group-btn">--}}
												{{--<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>--}}
												{{--</span>--}}
                                    {{--</div>--}}
                                    <div class="input-group date form_datetime">
                                        <input type="text" class="form-control" readonly="" name="search_end" value="{{{ Session::get('search_end') }}}">
												<span class="input-group-btn">
												<button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
												</span>
                                    </div>
                                </div>


                        </div>
                        <div class="clearfix">
                        </div><br/>
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-2 form-text">
                                Sortuj wg:
                            </div>
                            <div class="col-md-2">
                                <select name="search_field" class="form-control" style="width: 165px !important;">
                                    @if(empty(Session::get('search_field')))
                                        <option value="id" selected>ID ogłoszenia</option>
                                        <option value="ending_at">Data zakończenia</option>
                                        <option value="ad_text">Treśc ogłoszenia</option>
                                    @else
                                        @if(Session::get('search_field') == "id")
                                            <option value="id" selected>ID ogłoszenia</option>
                                            <option value="ending_at">Data zakończenia</option>
                                            <option value="ad_text">Treśc ogłoszenia</option>
                                        @elseif(Session::get('ending_at') == "id")
                                            <option value="id">ID ogłoszenia</option>
                                            <option value="ending_at" selected>Data zakończenia</option>
                                            <option value="ad_text">Treśc ogłoszenia</option>
                                        @else
                                            <option value="id">ID ogłoszenia</option>
                                            <option value="ending_at">Data zakończenia</option>
                                            <option value="ad_text" selected>Treśc ogłoszenia</option>
                                        @endif
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-2">
                                <select name="search_direction" class="form-control" style="width: 165px !important;">
                                    @if(empty(Session::get('search_direction')))
                                        <option value="desc" selected>Malejąco</option>
                                        <option value="asc">Rosnąco</option>
                                    @else
                                        @if(Session::get('search_direction') == "desc")
                                            <option value="desc" selected>Malejąco</option>
                                            <option value="asc">Rosnąco</option>
                                        @else
                                            <option value="desc">Malejąco</option>
                                            <option value="asc" selected>Rosnąco</option>
                                        @endif
                                    @endif
                                </select>
                            </div>

                            <div class="col-md-1">
                                <input type="submit" class="btn default btn-m blue-stripe" value="szukaj">

                            </div>
                            <div class="col-md-1">
                                <a href="/panel/ads/search/reset" class="btn default btn-m red-stripe">reset</a>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-slack"></i>Ogłoszenia
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title="">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-condensed table-hover dataTable" id="frm_1">
                                <thead>
                                <tr>
                                    <th width="5%" class="table-checkbox sorting_disabled">
                                        <div class="checker"><span class=""><input type="checkbox" class="group-checkable .checkboxes" data-set="#frm_1 .checkboxes"></span>
                                    </th>
                                    <th width="20%">Kategoria wew.</th>
                                    <th width="30%">Ogłoszenie</th>
                                    <th width="30%">Data zakończenia</th>
                                    <th width="15%">Notatka</th>
                                    <th width="5%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($ads as $ad)
                                    <tr>
                                        <td>
                                            @if($ad->locked == 0)
                                                <input type="checkbox" name="ad[]" value="{{{ $ad->id }}}" class="checkboxes">
                                            @else
                                                &nbsp;&nbsp;<i class="fa fa-lock"></i><br/>
                                                @if($locked_admin_info == "ok")
                                                    [ {{{ $ad->locked_id }}} ]
                                                @endif
                                            @endif
                                        </td>
                                        <td>@if($ad->internal_category_id == 0)
                                                <span class="text-muted">[ nie wybrano kategorii ]</span>
                                            @else
                                                [{{{ $ad->internalCategory->name }}}
                                            @endif
                                        </td>
                                        {{--<td>{{{ \Illuminate\Support\Str::words($ad->ad_text, 8, ' [...]')  }}}</td>--}}
                                        <td>{{{ $ad->ad_text }}} {{{ $ad->price }}}</td>
                                        <td>[ {{{ $ad->edition->name }}} ] {{{ $ad->ending_at }}}</td>
                                        <td>{{{ $ad->note }}}
                                        <hr class="contact_hr">
                                            {{{ $ad->phone_1 }}} {{{ $ad->phone_2 }}} {{{ $ad->phone_3 }}} {{{ $ad->contact_email }}}
                                        </td>
                                        <td>
                                            @if($ad->locked == 0 )
                                                <a href="/panel/ads/{{{ $ad->id }}}/edit" class="btn default btn-xs green-stripe"><i class="fa fa-edit"></i> Edytuj</a>
                                            @else
                                                <a href="#" class="btn default btn-xs green-stripe link-disabled" disabled><i class="fa fa-edit"></i> Edytuj</a>
                                                @if($ad->locked_id == Auth::id())
                                                    <a href="/panel/ads/{{{ $ad->id }}}/unlock" class="btn default btn-xs yellow-stripe"><i class="fa fa-unlock"></i> Odblokuj</a>
                                                @endif
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <!-- BEGIN PORTLET-->
                        <form action="{{ Request::url() }}" method="post" id="number_form">
                            <div class="row">
                                <div class="col-md-1 col-sm-1">Pokaż</div>
                                <div class="col-md-11 col-sm-11">
                                    <select name="display_number" id="number_selector">
                                        @if($display_number == 10)
                                            <option value="10" selected>10</option>
                                        @else
                                            <option value="10">10</option>
                                        @endif
                                        @if($display_number == 30)
                                            <option value="30" selected>30</option>
                                        @else
                                            <option value="30">30</option>
                                        @endif
                                        @if($display_number == 50)
                                            <option value="50" selected>50</option>
                                        @else
                                            <option value="50">50</option>
                                        @endif
                                        @if($display_number == 100)
                                            <option value="100" selected>100</option>
                                        @else
                                            <option value="100">100</option>
                                        @endif

                                    </select> wyników | Znaleziono włącznie {{{ $number_of_items }}} ogłoszeń
                                </div>

                            </div>
                        </form>
                        <!-- END PORTLET-->
                    </div>
                    <div class="col-md-6 col-sm-6" style="text-align: right;">
                        <!-- BEGIN PORTLET-->
                        {{ $pagination }}
                        <!-- END PORTLET-->
                    </div>
                </div>
                <div class="clearfix">
                </div>
                <!-- refresh modal -->
                <div class="modal fade" id="refresh" tabindex="-1" role="basic" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Ponów ogłoszenia</h4>
                            </div>
                            <div class="modal-body">
                                <div class="note note-info">
                                    <h4 class="block">Uwaga! </h4>
                                    <p>
                                        Wybierasz NOWY termin zakończenia edycji wszystkich zaznaczonych ogłoszeń!
                                    </p>
                                </div>
                                Data zakończenia emisji:
                                <select name="last_edition_id" class="form-control" id="additional_editions">
                                    {{ $i=1 }}
                                    @foreach($all_editions as $edition1)
                                        <option value="{{{ $edition1->id }}}">{{{ $i }}} - [{{{ $edition1->name }}}] do {{{ $edition1->end }}}</option>
                                        {{ $i++ }}
                                    @endforeach
                                </select>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn default" data-dismiss="modal">Zamknij</button>
                                <button type="button" class="btn blue" id="btn_refresh">Ponów</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- end refresh modal -->
                <!-- delete modal -->
                <div class="modal fade" id="delete" tabindex="-1" role="basic" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Usuń ogłoszenia</h4>
                            </div>
                            <div class="modal-body" id="del_mod">
                                <div class="note note-info">
                                    <h4 class="block">Uwaga! </h4>
                                    <p>
                                        Tej operacji nie da się cofnąć!<br/>
                                        Jeżeli w czasie gdy czytasz ten komunikat inna osoba rozpoczęła edycję któregoś z ogłoszeń jakie wybrano do usunięcia to NIE zostanie ono usunięte podczas tej operacji i NIE zostaniesz o tym powiadomiony!
                                    </p>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn default" data-dismiss="modal">Zamknij</button>
                                <button type="button" class="btn red" id="btn_delete">Usuń wybrane ogłoszenia!</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- end delete modal -->
                <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        @include('partial.footer')
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/respond.min.js"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{{ Config::get('app.url') }}}/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/ui-confirmations.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
{{--<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/components-pickers.js"></script>--}}




<script src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/table-managed.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        Index.initCalendar(); // init index page's custom scripts

        var table = $('#frm_1');
        var tableWrapper = jQuery('#frm_1_wrapper');
        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    //$(this).parents('tr').addClass("active");
                } else {
                    $(this).attr("checked", false);
                    //$(this).parents('tr').removeClass("active");
                }
            });
            jQuery.uniform.update(set);
        });

        table.on('change', 'tbody tr .checkboxes', function () {
           // $(this).parents('tr').toggleClass("active");
        });

        tableWrapper.find('.dataTables_length select').addClass("form-control input-xsmall ");

//        if (jQuery().datepicker) {
//            $('.date-picker').datepicker({
//                rtl: Metronic.isRTL(),
//                orientation: "left",
//                autoclose: true
//            });
//            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
//        }
        if (jQuery().datetimepicker) {
            $(".form_datetime").datetimepicker({
                weekStart: '1',
                language: 'pl',
                autoclose: true,
                isRTL: Metronic.isRTL(),
                format: "yyyy-mm-dd hh:ii:ss",
                pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left")
            });
        }

        $( "#number_selector" ).change(function() {
            $( "#number_form" ).submit();
        });
        var selected = [];
        $('#ads_refresh').click(function(event) {
           event.preventDefault();

            $('.checkboxes:checkbox:checked').each(function() {
                selected.push($(this).attr('value'));
            });
        });
        $('#ads_delete').click(function(event) {
            event.preventDefault();

            $('.checkboxes:checkbox:checked').each(function() {
                selected.push($(this).attr('value'));
            });
        });
        $('#ads_export_txt').click(function(event) {
            event.preventDefault();

            $('.checkboxes:checkbox:checked').each(function() {
                selected.push($(this).attr('value'));
            });
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });
            event.preventDefault();
            var number = Math.floor(Math.random() * 666666666666666666666);
            var filename = number;
            console.log(filename);
            var jqxhr = $.post( "/panel/ads/export/txt", { selected: selected, filename: filename },  function() {

                location.href = '/panel/ads/export/getfile/'+filename;
            })

            .fail(function() {
               alert('Wystąpił błąd. Sprawdź czy wszystkie ogłoszenia przeznaczone do eksportu mają wybraną karegorię!');
                        location.reload();
            })

        });

        $('.link-disabled').click(function(event) {
            event.preventDefault();
            return false;
        });

        $('#btn_delete').click(function(event) {
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });
            event.preventDefault();
            $('#del_mod').html('<i class="fa fa-spinner fa-spin"></i> czekaj ....');
            var jqxhr = $.post( "/panel/ads/delete", { selected: selected },  function() {
                location.reload();
            })

            .fail(function() {
                $('#del_mod').html('Wystąpił błąd.');
                $('#btn_delete').hide();
            })
        });

        $('#btn_refresh').click(function(event) {
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });
            event.preventDefault();
            var new_editions = $('#additional_editions').val();
            if(new_editions > 0) {
                $('.modal-body').html('<i class="fa fa-spinner fa-spin"></i> czekaj ....');
                var jqxhr = $.post( "/panel/ads/refresh", { selected: selected, editions: new_editions },  function() {
                    location.reload();
                })

                        .fail(function() {
                            $('.modal-body').html('Wystąpił błąd.');
                            $('#btn_refresh').hide();
                        })

            }else{
                alert('Wybierz zakończenie emisji.');
            }
        });
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>