@include('partial.header')
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="index.html">
                <img src="{{{ Config::get('app.url') }}}/assets/admin/layout/img/logo.png" alt="logo" class="logo-default"/>
            </a>
            <div class="menu-toggler sidebar-toggler hide">
            </div>
        </div>
        <!-- END LOGO -->
        @include('partial.topmenu')
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    @include('partial.menu')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="/panel">Panel główny</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="/panel/edition">Wydania</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="/panel/edition/{{ $edition->id }}/edit">Edytuj wydanie</a>
                    </li>

                </ul>
                <div class="page-toolbar">

                </div>

            </div>



            @if( Session::has('notice') )
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <strong>Info!</strong> {{{ Session::get('notice') }}}
                </div>
            @endif
            @if(!$errors->isEmpty())
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <strong>Błąd!</strong> {{ $errors->default->first() }}
                </div>
                @endif
                        <!-- END PAGE HEADER-->
                <!-- PAGE CONTRNTENT -->

                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-plus"></i>Dodaj wydanie
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title="">
                            </a>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <form action="/panel/edition/{{ $edition->id }}/edit" class="form-horizontal" method="post">
                            {{ Form::token(); }}
                            <input type="hidden" name="start" id="start" value="{{ $edition->start  }}">
                            <input type="hidden" name="end" id="end" value="{{ $edition->end }}">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Nazwa wydania:</label>
                                    <div class="col-md-4">
                                        <input type="text" name="name" class="form-control" value="{{ $edition->name }}">
                                    </div>
                                    <div class="col-md-4">
                                        <span class="text-muted"></span>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>

                                <div class="form-group cal-hide">
                                    <label class="col-md-3 control-label">Start wydania:</label>
                                    <div class="col-md-4">
                                        {{ $edition->start }}
                                    </div>
                                    <div class="col-md-4">
                                        <span class="text-muted"></span>
                                    </div>
                                </div>

                                <div class="clearfix">
                                </div>

                                <div class="form-group cal-hide">
                                    <label class="col-md-3 control-label">Koniec wydania:</label>
                                    <div class="col-md-4">
                                        {{ $edition->end }}
                                    </div>
                                    <div class="col-md-4">
                                        <span class="text-muted"></span>
                                    </div>
                                </div>

                                <div class="clearfix">
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Zmień zakres wydania:</label>
                                    <div class="col-md-4">
                                        <div id="reportrange" class="btn default">
                                            <i class="fa fa-calendar"></i>
                                            &nbsp; <span>{{ $start_date }} 11:00:00 - {{ $end_date }} 10:59:59</span>
                                            <b class="fa fa-angle-down"></b>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <span class="text-danger"><b>Uwaga!</b> Upewnij się, że wiesz co robisz, jeśli zmieniasz datę końcową wydania zmiana zostanie uwzględniona tylko dla tych ogłoszeń, które kończą się na tym wydaniu. Zmiana daty początkowej nie ma wpływu na wyświetlenia ogłoszeń. Upewnij się, że nie ma przerw pomiędzy wydaniami!</span>
                                    </div>
                                </div>

                                <div class="clearfix">
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"></label>
                                    <div class="col-md-4">
                                        <input type="submit" class="btn green-meadow" value="Zmień wydanie">
                                    </div>
                                    <div class="col-md-4">

                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>

                </div>


                <div class="clearfix">
                </div>

                <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        @include('partial.footer')
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/respond.min.js"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{{ Config::get('app.url') }}}/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/ui-confirmations.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>

<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}//assets/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/components-pickers.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
        Index.init();
        Index.initDashboardDaterange();
        Index.initJQVMAP(); // init index page's custom scripts
        Index.initCalendar(); // init index page's custom scripts
        Index.initCharts(); // init index page's custom scripts
        Index.initChat();
        Index.initMiniCharts();
        Tasks.initDashboardWidget();

//        ComponentsPickers.init();


        $('#reportrange').daterangepicker({
                    opens: (Metronic.isRTL() ? 'left' : 'right'),
                    startDate: '{{ $start_date }}',
                    endDate: '{{ $end_date }}',
                    minDate: '{{ $start_date }}',
                    maxDate: '12/31/2020',

                    showDropdowns: true,
                    showWeekNumbers: true,
                    timePicker: false,
                    timePickerIncrement: 1,
                    timePicker12Hour: false,
                    timePickerSeconds: false,

                    buttonClasses: ['btn'],
                    applyClass: 'green',
                    cancelClass: 'default',
                    format: 'DD/MM/YYYY',
                    separator: ' do ',
                    locale: {
                        applyLabel: 'OK',
                        fromLabel: 'OD',
                        toLabel: 'DO',
                        customRangeLabel: 'Wybierz zakres',
                        daysOfWeek: ['Nie', 'Po', 'Wt', 'Śr', 'Czw', 'Pi', 'So'],
                        monthNames: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
                        firstDay: 1
                    }
                },
                function (start, end) {
                    $('#reportrange span').html(start.format('DD/MM/YYYY') + ' 10:59:59 - ' + end.format('DD/MM/YYYY')+' 11:00:00');
                    $('#start').val(start.format('YYYY-MM-DD')+' 10:59:59');
                    $('#end').val(end.format('YYYY-MM-DD')+' 11:00:00');
                    $('.cal-hide').fadeOut();
                }
        );


    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>