<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Logowanie do systemu</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="{{{ Config::get('app.url') }}}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{{ Config::get('app.url') }}}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{{ Config::get('app.url') }}}/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{{ Config::get('app.url') }}}/assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="{{{ Config::get('app.url') }}}/assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="{{{ Config::get('app.url') }}}/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="{{{ Config::get('app.url') }}}/assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
    <link href="{{{ Config::get('app.url') }}}/assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="{{{ Config::get('app.url') }}}/assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->

<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="forget-form" action="{{ URL::to('/users/forgot_password') }}" method="post" style="display: block">
        <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
        <h3>Zapomniałeś hasła ?</h3>
        <p>
            Wprowadź swój adres email w polu poniżej aby otrzymać email z linkiem resetującym hasło.
        </p>
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                <button class="close" data-close="alert"></button>
                <span>{{{ Session::get('error') }}}</span></div>
        @endif

        @if (Session::get('notice'))
            <div class="alert alert-info">{{{ Session::get('notice') }}}</div>
        @endif
        <div class="form-group">
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" id="email" value="{{{ Input::old('email') }}}">
        </div>
        <div class="form-actions">
            <a href="/users/login" type="button" class="btn btn-default">Wstecz</a>
            <button type="submit" class="btn btn-success uppercase pull-right">Wyślij</button>
        </div>
    </form>
    <!-- END FORGOT PASSWORD FORM -->

</div>
<div class="copyright">
    2015 © ernes.pl
</div>
<!-- END LOGIN -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/respond.min.js"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{{ Config::get('app.url') }}}/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/login.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Login.init();
        Demo.init();
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>