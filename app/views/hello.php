<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ernes.pl - work in progress</title>
	<style>
		@import url(//fonts.googleapis.com/css?family=Lato:700);

		body {
			margin:0;
			font-family:'Lato', sans-serif;
			text-align:center;
			color: #999;
		}

		.welcome {
			width: 300px;
			height: 200px;
			position: absolute;
			left: 50%;
			top: 50%;
			margin-left: -150px;
			margin-top: -100px;
		}

		a, a:visited {
			text-decoration:none;
		}

		h1 {
			font-size: 32px;
			margin: 16px 0 0 0;
		}
	</style>
</head>
<body>
	<div class="welcome">
		<a href="http://ernes.pl" title="ernes.pl usługi informatyczne"><img src="http://ernes.pl/theme/Ernes/images/logo.png" alt="ernes"></a>
		<h1>ernes.pl - work in progress</h1>
	</div>
</body>
</html>
