@include('partial.header')
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="index.html">
                <img src="{{{ Config::get('app.url') }}}/assets/admin/layout/img/logo.png" alt="logo" class="logo-default"/>
            </a>
            <div class="menu-toggler sidebar-toggler hide">
            </div>
        </div>
        <!-- END LOGO -->
        @include('partial.topmenu')
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    @include('partial.menu')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="/panel">Panel główny</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="/panel/numbers">Numery zastrzeżone</a>
                    </li>

                </ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="true">
                            Opcje <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <a href="/panel/numbers/add"><i class="fa  fa-plus"></i>
                                    Dodaj</a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>



            @if( Session::has('notice') )
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <strong>Info!</strong> {{{ Session::get('notice') }}}
                </div>
            @endif
            @if(!$errors->isEmpty())
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <strong>Błąd!</strong>
                    @foreach( $errors->all() as $error)
                        <li> {{{ $error }}}</li>
                    @endforeach
                </div>
                @endif
                        <!-- END PAGE HEADER-->
                <!-- PAGE CONTRNTENT -->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-slack"></i>Numery zastrzeżone
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title="">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-condensed table-hover dataTable" id="frm_1">
                                <thead>
                                <tr>
                                    <th width="5%">ID</th>
                                    <th width="20%">Numer</th>
                                    <th width="30%">Powód</th>
                                    <th width="30%">Data dodania</th>
                                    <th width="15%">Opcje</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($numbers as $number)
                                    <tr>
                                        <td>{{{ $number->id }}}</td>
                                        <td>{{{ $number->phone }}}</td>
                                        <td>{{{ $number->reason }}}</td>
                                        <td>{{{ $number->created_at }}}</td>
                                        <td>
                                            <a href="/panel/numbers/{{{ $number->id }}}/edit" class="btn default btn-xs green-stripe"><i class="fa fa-edit"></i> Edytuj</a>
                                            <a href="/panel/numbers/{{{ $number->id }}}/delete" class="btn default btn-xs red-stripe" data-toggle="confirmation" data-popout="true" title="" data-original-title="Czy potwierdzasz usunięcie?"><i class="fa fa-trash"></i> Usuń</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <!-- BEGIN PORTLET-->

                        <!-- END PORTLET-->
                    </div>
                    <div class="col-md-6 col-sm-6" style="text-align: right;">
                        <!-- BEGIN PORTLET-->
                        {{ $pagination }}
                        <!-- END PORTLET-->
                    </div>
                </div>
                <div class="clearfix">
                </div>
                <!-- refresh modal -->
                <div class="modal fade" id="refresh" tabindex="-1" role="basic" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Ponów ogłoszenia</h4>
                            </div>
                            <div class="modal-body">
                                <div class="note note-info">
                                    <h4 class="block">Uwaga! </h4>
                                    <p>
                                        Podaj ilość wydań o jaką zostanie przedłużona emisja.<br/>
                                        Ilość wydań liczona jest dla każdego ogłoszenia z osobna (tzn. jeśli ogłoszenie "A" ma ustawione ostatnie wydanie na 10, a ogłoszenie "B" na 15 przedłużając emisję o 5 ogłoszenie "A" będzie kończyło się na 15 wydaniu a ogłoszenie "B" na 20).
                                        Wprowadzone dane zostaną zastosowane tylko w przypadku gdy wydania rzeczywiście znajdują się w systemie (nie można dodać ilości awansem).<br/>
                                        System NIE przedłuża ogłoszeń jeśli wybrano za wysoką wartość! Dlatego w przypadku gdy przedłużenie będzie skuteczne jedynie dla części z wybranych ogłoszeń NIE zostaniesz powiadomiony o tych których nie udało się przedłużyć.<br/>
                                    </p>
                                </div>
                                Podaj ilość dodatkowych emisji: <input type="text" class="form-control" id="additional_editions" value="1">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn default" data-dismiss="modal">Zamknij</button>
                                <button type="button" class="btn blue" id="btn_refresh">Ponów</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- end refresh modal -->
                <!-- delete modal -->
                <div class="modal fade" id="delete" tabindex="-1" role="basic" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Usuń ogłoszenia</h4>
                            </div>
                            <div class="modal-body" id="del_mod">
                                <div class="note note-info">
                                    <h4 class="block">Uwaga! </h4>
                                    <p>
                                        Tej operacji nie da się cofnąć!<br/>
                                        Jeżeli w czasie gdy czytasz ten komunikat inna osoba rozpoczęła edycję któregoś z ogłoszeń jakie wybrano do usunięcia to NIE zostanie ono usunięte podczas tej operacji i NIE zostaniesz o tym powiadomiony!
                                    </p>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn default" data-dismiss="modal">Zamknij</button>
                                <button type="button" class="btn red" id="btn_delete">Usuń wybrane ogłoszenia!</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- end delete modal -->
                <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        @include('partial.footer')
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/respond.min.js"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{{ Config::get('app.url') }}}/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/ui-confirmations.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{{ Config::get('app.url') }}}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/table-managed.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
        Index.init();
        Index.initDashboardDaterange();
        Index.initJQVMAP(); // init index page's custom scripts
        Index.initCalendar(); // init index page's custom scripts
        Index.initCharts(); // init index page's custom scripts
        Index.initChat();
        Index.initMiniCharts();
        Tasks.initDashboardWidget();

    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>