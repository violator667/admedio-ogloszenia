@include('partial.header')
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="index.html">
                <img src="{{{ Config::get('app.url') }}}/assets/admin/layout/img/logo.png" alt="logo" class="logo-default"/>
            </a>
            <div class="menu-toggler sidebar-toggler hide">
            </div>
        </div>
        <!-- END LOGO -->
        @include('partial.topmenu')
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    @include('partial.menu')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="/panel">Panel główny</a>
                    </li>

                </ul>
                <div class="page-toolbar">

                </div>
            </div>
            <h3 class="page-title">
                Panel główny <small>statystyki</small>
            </h3>
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS -->
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-comments"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{{ $new_ads }}}
                            </div>
                            <div class="desc">
                                Nowych ogłoszeń
                            </div>
                        </div>
                        <a class="more" href="/panel/ads/new">
                            nowe <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat red-intense">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{{ $active_ads }}}
                            </div>
                            <div class="desc">
                                Aktywnych ogłoszeń
                            </div>
                        </div>
                        <a class="more" href="/panel/ads/active">
                            aktywne <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-steel">
                        <div class="visual">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{{ $archive_ads }}}
                            </div>
                            <div class="desc">
                                Archiwalnych ogłoszeń
                            </div>
                        </div>
                        <a class="more" href="/panel/ads/archived">
                            archiwum <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat green-jungle">
                        <div class="visual">
                            <i class="fa fa-bullhorn"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{{ $all_ads }}}
                            </div>
                            <div class="desc">
                                Wszystkich ogłoszeń
                            </div>
                        </div>
                        <a class="more" href="/panel/ads">
                            wszystkie <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat green-seagreen">
                        <div class="visual">
                            <i class="fa fa-folder-open"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{{ $cat_num }}}
                            </div>
                            <div class="desc">
                                Kategorii wewnętrzych
                            </div>
                        </div>
                        <a class="more" href="/panel/internal-category">
                            kategorie <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-sharp">
                        <div class="visual">
                            <i class="fa fa-file-text"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{{ $editions_num }}}
                            </div>
                            <div class="desc">
                                Wydań
                            </div>
                        </div>
                        <a class="more" href="/panel/edition">
                            wydania <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat green-haze">
                        <div class="visual">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{{ $users_num }}}
                            </div>
                            <div class="desc">
                                Użytkowników
                            </div>
                        </div>
                        <a class="more" href="/panel/users">
                            użytkownicy <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat purple-plum">
                        <div class="visual">
                            <i class="fa fa-bolt"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{{ $mistakes_num }}}
                            </div>
                            <div class="desc">
                                Błędów ogółem
                            </div>
                        </div>
                        <a class="more" href="/panel/ads">
                            ogłoszenia <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
            </div>
            <!-- END DASHBOARD STATS -->
            <div class="clearfix">
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">

                </div>
                <div class="col-md-6 col-sm-6">

                </div>
            </div>
            <div class="clearfix">
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        @include('partial.footer')
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/respond.min.js"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{{ Config::get('app.url') }}}/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="{{{ Config::get('app.url') }}}/assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
        Index.init();
        Index.initDashboardDaterange();
        Index.initJQVMAP(); // init index page's custom scripts
        Index.initCalendar(); // init index page's custom scripts
        Index.initCharts(); // init index page's custom scripts
        Index.initChat();
        Index.initMiniCharts();
        Tasks.initDashboardWidget();
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>