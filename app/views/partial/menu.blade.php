<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <li class="sidebar-search-wrapper">
                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                <form class="sidebar-search" action="/panel/ads/search/stay" method="POST">
                    {{ Form::token() }}
                    <a href="javascript:;" class="remove">
                        <i class="icon-close"></i>
                    </a>
                    <div class="input-group">
                        <input type="text" name="search_text" class="form-control" placeholder="Wyszukaj...">
							<span class="input-group-btn">

							<button class="btn submit"><i class="icon-magnifier"></i></button>
							</span>
                    </div>
                </form>
                <!-- END RESPONSIVE QUICK SEARCH FORM -->
            </li>


            @if( $menuPosition == 'panel')
                <li class="start active">
            @else
                <li class="start">
            @endif
                <a href="/panel">
                    <i class="icon-home"></i>
                    <span class="title">Panel główny</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
            </li>

            @if( $menuPosition == 'ads-add')
                <li class="start active">
            @else
                <li class="start">
            @endif
                    <a href="/panel/ads/add">
                        <i class="icon-plus"></i>
                        <span class="title">Dodaj ogłoszenie</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>

            @if( $menuPosition == 'ads')
                <li class="active open">
            @else
                <li>
            @endif
                    <a href="javascript:;">
                        <i class="fa fa-book"></i>
                        <span class="title">Ogłoszenia</span>
                        <span class="arrow  open"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="/panel/ads/active"><i class="fa  fa-check"></i>
                                Aktualne</a>
                        </li>
                        <li>
                            <a href="/panel/ads/new"><i class="fa  fa-download"></i>
                                Do edycji</a>
                        </li>
                        <li>
                            <a href="/panel/ads/archived"><i class="fa  fa-calendar"></i>
                                Archiwum</a>
                        </li>
                        <li>
                            <a href="/panel/ads/"><i class="fa fa-folder"></i>
                                Wszystkie</a>
                        </li>

                        <li>
                            <a href="/panel/ads/export/txt"><i class="fa fa-cloud-upload"></i>
                                Eksport do TXT</a>
                        </li>
                    </ul>
                </li>

            @if( $menuPosition == 'cms')
                <li class="active open last">
            @else
                <li class="last">
                    @endif
                    <a href="javascript:;">
                        <i class="fa fa-warning"></i>
                        <span class="title">Informacje</span>
                        <span class="arrow  open"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="/panel/cms/"><i class="fa fa-clipboard"></i>
                                Wszystkie strony</a>
                        </li>
                        @if(Auth::user()->can('cms_manage'))
                            <li>
                                <a href="/panel/cms/add/"><i class="fa fa-edit"></i>
                                    Utwórz stronę</a>
                            </li>
                        @endif
                    </ul>
                </li>

            @if( $menuPosition == 'ads-internet')
                <li class="active open">
            @else
                <li>
            @endif
                    <a href="javascript:;">
                        <i class="fa fa-globe"></i>
                        <span class="title">Internet</span>
                        <span class="arrow  open"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="/panel/ads/exported"><i class="fa  fa-upload"></i>
                                Wyeksportowane</a>
                        </li>
                        <li>
                            <a href="/panel/ads/import"><i class="fa  fa-cloud-download"></i>
                                Importuj</a>
                        </li>
                    </ul>
                </li>

            @if( $menuPosition == 'category')
                <li class="active open">
            @else
                <li>
            @endif
                <a href="javascript:;">
                    <i class="fa fa-sitemap"></i>
                    <span class="title">Dział</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="/panel/category"><i class="fa fa-th-large"></i>
                            Wszystkie</a>
                    </li>
                    {{--<li>--}}
                        {{--<a href="/panel/category/add"><i class="fa fa-plus"></i>--}}
                            {{--Dodaj</a>--}}
                    {{--</li>--}}
                </ul>
            </li>

            @if( $menuPosition == 'category2')
                <li class="active open">
            @else
                <li>
                    @endif
                    <a href="javascript:;">
                        <i class="fa fa-tags"></i>
                        <span class="title">Kategorie wew.</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="/panel/internal-category"><i class="fa fa-th-large"></i>
                                Wszystkie</a>
                        </li>
                        <li>
                        <a href="/panel/internal-category/add"><i class="fa fa-plus"></i>
                        Dodaj</a>
                        </li>
                    </ul>
                </li>



            @if( $menuPosition == 'edition')
                <li class="active open">
            @else
                <li>
                    @endif
                    <a href="javascript:;">
                        <i class="icon-docs"></i>
                        <span class="title">Wydania</span>
                        <span class="arrow  open"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="/panel/edition/"><i class="fa fa-calendar"></i>
                                Wszystkie</a>
                        </li>
                        <li>
                            <a href="/panel/edition/add"><i class="fa  fa-plus"></i>
                                Dodaj</a>
                        </li>

                    </ul>
                </li>

            @if( $menuPosition == 'numbers')
                <li class="active open">
            @else
                <li>
                    @endif
                    <a href="javascript:;">
                        <i class="fa fa-phone"></i>
                        <span class="title">Numery zastrzeżone</span>
                        <span class="arrow  open"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="/panel/numbers/"><i class="fa fa-database"></i>
                                Wszystkie</a>
                        </li>
                        <li>
                            <a href="/panel/numbers/add"><i class="fa  fa-plus"></i>
                                Dodaj</a>
                        </li>

                    </ul>
                </li>

            @if( $menuPosition == 'users')
                <li class="active open">
            @else
                <li>
            @endif
                <a href="javascript:;">
                    <i class="icon-user"></i>
                    <span class="title">Użytkownicy</span>
                    <span class="arrow  open"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="/panel/users/"><i class="fa fa-users"></i>
                            Wszyscy</a>
                    </li>
                    <li>
                        <a href="/panel/users/add"><i class="fa fa-plus"></i>
                            Dodaj</a>
                    </li>
                </ul>
            </li>

            @if( $menuPosition == 'message')
                <li class="active open last">
            @else
                <li class="last">
                    @endif
                    <a href="javascript:;">
                        <i class="icon-envelope"></i>
                        <span class="title">Wiadomości</span>
                        <span class="arrow  open"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="/panel/messages/"><i class="fa fa-envelope"></i>
                                Inbox
                                <span class="badge badge-danger">{{ $messages_number }}</span></a>
                        </li>
                        @if(Auth::user()->can('message_send'))
                            <li>
                                <a href="/panel/messages/send-to-user-id/"><i class="fa fa-edit"></i>
                                    Napisz</a>
                            </li>
                        @endif
                    </ul>
                </li>


        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->