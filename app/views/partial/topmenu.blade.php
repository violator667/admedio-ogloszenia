<!-- BEGIN RESPONSIVE MENU TOGGLER -->
<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
</a>
<!-- END RESPONSIVE MENU TOGGLER -->
<!-- BEGIN TOP NAVIGATION MENU -->
<div class="top-menu">
    <ul class="nav navbar-nav pull-right">
        <!-- BEGIN INBOX DROPDOWN -->
        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
        <li class="edition_bar">Aktualne wydanie: {{{ $current_edition->name }}} do {{{ $current_edition->end }}}</li>
        <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <i class="icon-envelope-open"></i>
					<span class="badge badge-default">
					{{{ $messages_number }}} </span>
            </a>
            <ul class="dropdown-menu">
                <li class="external">
                    <h3><span class="bold">{{{ $messages_number }}} Nowych</span> Wiadomości</h3>
                    <a href="/panel/messages/">[wszystkie]</a>
                </li>
                @if($messages_number > 0)
                <li>
                    <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                        @foreach($messages_unread as $msg)
                            <li>
                                <a href="/panel/messages/show/{{ $msg->id }}">
									<span class="photo">
									    <i class="fa fa-envelope"></i>
									</span>
									<span class="subject">
									<span class="from">
									{{ $msg->subject }}</span>
									<span class="time">{{ $msg->created_at }}</span>
									</span>
									<span class="message">
									{{{ Str::words(strip_tags($msg->body), 10) }}}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                @endif
            </ul>
        </li>
        <!-- END INBOX DROPDOWN -->

        <!-- BEGIN USER LOGIN DROPDOWN -->
        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
        <li class="dropdown dropdown-user">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

					<span class="username username-hide-on-mobile">
					{{ Auth::user()->name }} </span>
                <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-default">
                <li>
                    <a href="/panel/messages/">
                        <i class="icon-envelope-open"></i> Wiadomości <span class="badge badge-danger">{{ $messages_number }}</span>
                    </a>
                </li>

                <li class="divider">
                </li>

                <li>
                    <a href="/users/logout">
                        <i class="icon-key"></i> Wyloguj </a>
                </li>
            </ul>
        </li>
        <!-- END USER LOGIN DROPDOWN -->
        <!-- BEGIN QUICK SIDEBAR TOGGLER -->
        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
        <li class="dropdown dropdown-quick-sidebar-toggler">
            <a href="/users/logout" class="dropdown-toggle">
                <i class="icon-logout"></i>
            </a>
        </li>
        <!-- END QUICK SIDEBAR TOGGLER -->
    </ul>
</div>
<!-- END TOP NAVIGATION MENU -->